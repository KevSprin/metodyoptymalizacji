﻿using System;
using System.Collections.Generic;
using System.Linq;
using BibliotekaGraf;

namespace CyklEulera
{
    public class CyklEulera<T, K>
    {
        private GrafAbstrakcyjny<T, K> graf;
        private Stack<T> result;
        public Stack<T> Expand(T node)
        {
            if (result.Count > 0)
            {
                T previous_node = result.Peek();
                graf.OdwiedzamKrawedz(graf.ZwrocKrawedz(previous_node, node));
            }
            result.Push(node);

            List<T> sasiedzi = graf.ZwrocSasiadowNieodwiedzonychKrawedzi(node).ToList();
            Stack<T> expand_stack = new Stack<T>();
            using (var iterator = sasiedzi.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    expand_stack.Push(iterator.Current);
                }
            }
            return expand_stack;
        }

        public bool EndCondition(T node, T goal)
        {
            if (node.Equals(goal) && result.Count.Equals(graf.ZwrocWierzholki().ToList().Count + 1))
            {
                result.Push(node);
                return true;
            }
            else return false;
        }

        public bool DFS(T node, T goal)
        {
            if (EndCondition(node, goal))
            {
                return true;
            }
            Stack<T> stos = Expand(node);
            while (stos.Count > 0)
            {
                var popped_node = stos.Pop();
                return DFS(popped_node, goal);
            }
            return false;
        }

        public Stack<T> Traverse()
        {
            if (!CheckDegree()) throw new ArgumentException("Stopień parzystości się nie zgadza!");
            foreach (var node in graf.ZwrocWierzholki())
            {
                if (DFS(node, node)) return result;
            }
            throw new ArgumentException("Nie ma wyniku!");
        }

        public bool CheckDegree()
        {
            foreach (var node in graf.ZwrocWierzholki().ToList())
            {
                if (graf.Stopien(node) % 2 != 0) return false; 
            }
            return true;
        }

        public CyklEulera(GrafAbstrakcyjny<T, K> graf)
        {
            this.graf = graf;
            result = new Stack<T>();
        }
    }
}
