﻿using System;
using System.Collections.Generic;
using System.Linq;
using BibliotekaGraf;
using MaksymalneSkojarzenie;

namespace AlgorytmWegierski
{
    public class AlgorytmWegierski<T>
    {
        private GrafAbstrakcyjny<T, int> grafSkierowany;
        private GrafAbstrakcyjny<T, int> grafRowny;
        public AlgorytmWegierski(GrafSkierowany<T, int> grafSkierowany)
        {
            this.grafSkierowany = grafSkierowany;
            AlgorytmKuhnaMunkresa();
        }

        /// <summary>
        /// Funkcja do znajdywania początkowych etykiet dla wierzchołków
        /// </summary>
        private void ZnajdzPoczatkoweEtykiety()
        {
            // dla każdego y należącego do zbiory wierzchołków V2 dodajemy etykiete o wartości 0
            foreach (var wierzcholek in grafSkierowany.ZwrocZbiorV2())
            {
                grafSkierowany.DodajEtykiete(wierzcholek, 0);
            }
            foreach (var wierzcholek in grafSkierowany.ZwrocZbiorV1())
            {
                List<int> wagi_wierzcholka = new List<int>();
                // dla każdej krawędzi połączonej z danym wierzchołkiem
                foreach (var krawedz in grafSkierowany.ZwrocKrawedzieWierzcholka(wierzcholek))
                {
                    wagi_wierzcholka.Add(grafSkierowany.ZwrocWage(krawedz));
                }
                // dodaj do kolekcji etykiete z danym wierzchołkiem oraz etykietą, czyli największa waga ze wszystkich krawędzi 
                grafSkierowany.DodajEtykiete(wierzcholek, wagi_wierzcholka.Max());
            }
        }

        /// <summary>
        /// Funkcja przygotowująca graf równy.
        /// </summary>
        private void PrzygotujGrafRowny()
        {
            grafRowny = new GrafSkierowany<T, int>();
            grafRowny.KopiujSkojarzeniaZbiory(grafSkierowany);
            grafRowny.DodajWierzholek(grafSkierowany.ZwrocWierzholki());
            foreach (var wierzcholek in grafRowny.ZwrocZbiorV1())
            {
                foreach (var krawedz in grafSkierowany.ZwrocKrawedzRownejWarunkowi(wierzcholek))
                {
                    grafRowny.DodajKrawedz(krawedz);
                }
            }
            foreach (var skojarzenie in grafRowny.ZwrocSkojarzenia())
            {
                IPara<T> tmp = skojarzenie.Clone();
                tmp.Zamiana();
                var krawedz = grafRowny.ZwrocKrawedzie().FirstOrDefault(x => x.Equals(tmp));
                if (krawedz != null)
                {
                    grafRowny.UsunKrawedz(tmp);
                    grafRowny.DodajKrawedz(skojarzenie);
                }
            }
            grafRowny.KopiujWagi(grafSkierowany);
        }

        /// <summary>
        /// Funkcja zwracająca graf.
        /// </summary>
        /// <returns></returns>
        public IGraf<T ,int> ZwrocGraf()
        {
            return grafRowny;
        }

        /// <summary>
        /// Funkcja realizująca Algorytm Kuhna Munkresa.
        /// </summary>
        public void AlgorytmKuhnaMunkresa()
        {
            // Generujemy początkowe etykiety oraz skojarzenie
            ZnajdzPoczatkoweEtykiety();
            

            PrzygotujGrafRowny();
            MaksymalneSkojarzenie<T, int> maksymalneSkojarzenie = new MaksymalneSkojarzenie<T, int>(grafRowny);
            maksymalneSkojarzenie.ZnajdzSkojarzeniePoczatkowe();
            grafRowny = maksymalneSkojarzenie.ZwrocGraf();
            grafSkierowany.DodajSkojarzenie(grafRowny.ZwrocSkojarzenia());

            // Krok pierwszy - jeśli posiadamy pełne skojarzenie to nie wchodzimy
            while (grafRowny.ZwrocZbiorWolnychV1().Count() > 0 && grafRowny.ZwrocZbiorWolnychV2().Count() > 0)
            {
                // w przeciwnym razie ustawiamy wierzchołek wolny w S={v} oraz T=0 
                IList<T> zbior_S = new List<T>();
                IList<T> zbior_T = new List<T>();
                zbior_S.Add(grafRowny.ZwrocZbiorWolnychV1().First());
                while (true)
                {
                    // Krok drugi - obliczamy alfe_l 
                    if(!SasiedziOdSZawarteWT(zbior_S, zbior_T))
                    {
                        int alfa = ObliczanieAlfy(zbior_S, zbior_T);
                        foreach (var wierzcholek in grafSkierowany.ZwrocWierzholki())
                        {
                            if (zbior_S.Contains(wierzcholek))
                            {
                                var nowa_etykieta = grafSkierowany.ZwrocEtykiete(wierzcholek) - alfa;
                                grafSkierowany.UsunEtykiete(wierzcholek);
                                grafSkierowany.DodajEtykiete(wierzcholek, nowa_etykieta);
                            }
                            else if(zbior_T.Contains(wierzcholek))
                            {
                                var nowa_etykieta = grafSkierowany.ZwrocEtykiete(wierzcholek) + alfa;
                                grafSkierowany.UsunEtykiete(wierzcholek);
                                grafSkierowany.DodajEtykiete(wierzcholek, nowa_etykieta);
                            }
                        }
                        PrzygotujGrafRowny();
                    }

                    // Krok trzeci - wybieramy y ze zbioru N(S) - T
                    var wierzcholek_y = ZnajdzWierzcholekNalezaceDoSminusT(zbior_S, zbior_T);
                    // Jeśli y jest skojarzone to wybieramy wierzchołki z skierowania y i z i dodajemy do zbiorów S oraz T. S = S + {z}; T = T + {y}
                    if (grafRowny.IsSkojarzony(wierzcholek_y))
                    {
                        var skojarzenie = grafRowny.ZwrocSkojarzenie(wierzcholek_y);
                        zbior_S.Add(skojarzenie.ZwrocDrugi());
                        zbior_T.Add(wierzcholek_y);
                        // wraca do kroku drugiego
                    }
                    else
                    {
                        MaksymalneSkojarzenie<T, int> maksymalne_skojarzenie = new MaksymalneSkojarzenie<T, int>(grafRowny);
                        maksymalne_skojarzenie.ZnajdzMaksymalneSkojarzenie();
                        grafSkierowany.UsunSkojarzenia();
                        grafSkierowany.DodajSkojarzenie(grafRowny.ZwrocSkojarzenia());
                        break;
                        // wraca do kroku pierwszego
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja do obliczenia alfy aby uzyskać minimalną wartość
        /// </summary>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        private int ObliczanieAlfy(IList<T> S, IList<T> T)
        {
            List<int> values = new List<int>();
            foreach (var wierzcholek in S)
            {
                foreach (var sasiad in grafSkierowany.ZwrocSasiadow(wierzcholek))
                {
                    if (T.Contains(sasiad)) continue;
                    var lx = grafSkierowany.ZwrocEtykiete(wierzcholek);
                    var ly = grafSkierowany.ZwrocEtykiete(sasiad);
                    var w_xy = grafSkierowany.ZwrocWage(wierzcholek, sasiad);
                    var alfa = lx + ly - w_xy;
                    values.Add(alfa);
                }
            }
            return values.Min();
        }

        /// <summary>
        /// Funkcja sprawdzająca czy Sąsiedzi wierzchołków zbioru S należą do T
        /// </summary>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        private bool SasiedziOdSZawarteWT(IList<T> S, IList<T> T)
        {
            foreach (var wierzcholek in S)
            {
                foreach (var sasiad in grafRowny.ZwrocSasiadow(wierzcholek))
                {
                    if (T.Contains(sasiad)) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        private T ZnajdzWierzcholekNalezaceDoSminusT(IList<T> S, IList<T> T) 
        {
            var sasiedzi = ZnajdzSasiadow(S);
            var sasiedzi_bez_T = SasiedziMinusT(sasiedzi, T);
            T wierzcholek = sasiedzi_bez_T.FirstOrDefault();
            return wierzcholek;
        }

        /// <summary>
        /// Funkcja wykonująca operację odejmowania zbioru wierzchołków T od zbiory sąsiadów N(S), czyli N(S) - T
        /// </summary>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        private IList<T> SasiedziMinusT(IList<T> S, IList<T> T)
        {
            IList<T> result = new List<T>(S);
            foreach (var wierzcholek in T)
            {
                if (result.Contains(wierzcholek)) result.Remove(wierzcholek);
            }
            return result;
        }

        /// <summary>
        /// Funkcja zwracająca sasiadów podzbioru wierzchołków S, czyli N(S)
        /// </summary>
        /// <param name="S"></param>
        /// <returns></returns>
        private IList<T> ZnajdzSasiadow(IList<T> S)
        {
            IList<T> result = new List<T>();
            foreach (var wierzcholek in S)
            {
                foreach (var sasiad in grafRowny.ZwrocSasiadow(wierzcholek))
                {
                    if (!result.Contains(sasiad)) result.Add(sasiad);
                }
            }
            return result;
        }
    }
}
