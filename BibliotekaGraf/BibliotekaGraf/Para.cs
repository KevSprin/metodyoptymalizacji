﻿using System;

namespace BibliotekaGraf
{
    public class Para<T> : IPara<T>, ICloneable
    {
        private T _start;
        private T _end;

        public Para(T start, T end)
        {
            if (start == null || end == null) throw new ArgumentNullException();
            if (start.GetType() != end.GetType()) throw new ArgumentException();
            _start = start;
            _end = end;
        }

        /// <summary>
        /// Funkcja sprawdzająca czy Obiekt Para zawiera przekazywaną wartość.
        /// </summary>
        /// <param name="wartosc"></param>
        /// <returns>bool</returns>
        public bool Zawiera(T wartosc)
        {
            return wartosc.Equals(_start) || wartosc.Equals(_end);
        }

        /// <summary>
        /// Zwróć drugą wartość pary.
        /// </summary>
        /// <returns>Value from type T</returns>
        public T ZwrocDrugi()
        {
            return _end;
        }

        /// <summary>
        /// Zwróć pierwszą wartość pary.
        /// </summary>
        /// <returns></returns>
        public T ZwrocPierwszy()
        {
            return _start;
        }

        // metoda przesłonięta aby porównać oba wierzhołki z jednej pary z drugą
        /// <summary>
        /// Metoda porównująca obiekty typu Para
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Para<T>)) return false;
            Para<T> rzutowana = (Para<T>)obj;
            return rzutowana._start.Equals(_start) && rzutowana._end.Equals(_end);
        }

        // suma hashkodów wierzholka jeden i dwa
        public override int GetHashCode()
        {
            return _start.GetHashCode() + _end.GetHashCode();
        }

        /// <summary>
        /// Metoda zamieniająca wartości pary miejscami. W przypadku grafu skierowanego do zmiany kierunku.
        /// </summary>
        public void Zamiana()
        {
            T tmp = _start;
            _start = _end;
            _end = tmp;
        }

        /// <summary>
        /// Metoda tworząca nową parę z zamienionymi miejscami wartości
        /// </summary>
        /// <returns></returns>
        public IPara<T> ZwrocOdwroconaPare()
        {
            return new Para<T>(_end, _start);
        }

        public IPara<T> Clone()
        {
            return new Para<T>(_start, _end);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
