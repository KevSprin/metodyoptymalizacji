﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BibliotekaGraf
{
    public abstract class GrafAbstrakcyjny<T, K> : IGraf<T, K>
    {
        protected List<T> Wierzholki = new List<T>();
        protected List<IPara<T>> Krawedzie = new List<IPara<T>>();
        protected Dictionary<IPara<T>, K> Wagi = new Dictionary<IPara<T>, K>();
        protected List<IPara<T>> Skojarzenia = new List<IPara<T>>();
        protected Dictionary<IPara<T>, bool> OdwiedzoneKrawedzie = new Dictionary<IPara<T>, bool>();
        protected Dictionary<T, bool> OdwiedzoneWierzholki = new Dictionary<T, bool>();
        protected List<T> Zbior_V1 = new List<T>();
        protected List<T> Zbior_V2 = new List<T>();
        protected Dictionary<T, K> Etykiety = new Dictionary<T, K>();
        protected Dictionary<T, K> Kolory = new Dictionary<T, K>();
        protected Dictionary<T, List<K>> Dopuszalne_Kolory = new Dictionary<T, List<K>>();
        protected Dictionary<T, K> Maksymalny_Kolor_Wierzcholka = new Dictionary<T, K>();

        /// <summary>
        /// Indexer do wierzchołków grafu
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get
            {
                return Wierzholki[index];
            }
        }

        /// <summary>
        /// Funkcja dodająca pojedyńczego wierzchołka do grafu.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>bool</returns>
        public bool DodajWierzholek(T wierzholek)
        {
            if (wierzholek == null) throw new ArgumentNullException();
            if (Wierzholki.Contains(wierzholek)) return false;
            Wierzholki.Add(wierzholek);
            OdwiedzoneWierzholki.Add(wierzholek, false);
            return true;
        }

        /// <summary>
        /// Funkcja dodająca cały zbiór wierzchołków do grafu.
        /// </summary>
        /// <param name="zbior_wierzholkow"></param>
        public void DodajWierzholek(IEnumerable<T> zbior_wierzholkow)
        {
            if (zbior_wierzholkow == null) throw new ArgumentNullException();
            using (var iterator = zbior_wierzholkow.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    if (iterator.Current != null && !Wierzholki.Contains(iterator.Current))
                    {
                        Wierzholki.Add(iterator.Current);
                        OdwiedzoneWierzholki.Add(iterator.Current, false);
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja dodająca krawędź do grafu. Przyjmuje jako parametr obiekt klasy Para.
        /// </summary>
        /// <param name="krawedz"></param>
        /// <returns>bool</returns>
        public bool DodajKrawedz(IPara<T> krawedz)
        {
            if (krawedz == null) throw new ArgumentNullException("Krawedz jest pusta!");
            Krawedzie.Add(krawedz);
            return true;
        }

        /// <summary>
        /// Funkcja dodająca skojarzenia do grafu. Jako parametry przyjmuje bezpośrednio wierzchołki.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        public void DodajSkojarzenie(T v1, T v2)
        {
            if (v1 == null || v2 == null) throw new ArgumentNullException("Conajmniej jeden z wierzcholkow jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) throw new ArgumentException("Conajmniej jeden z wierzchołków nie jest zawarty w zbiorze!");
            IPara<T> para = new Para<T>(v1, v2);
            Skojarzenia.Add(para);
        }

        /// <summary>
        /// Funkcja dodająca skojarzenia do grafu. Jako parametr orzyjmuje obiekt typu Para.
        /// </summary>
        /// <param name="skojarzenie"></param>
        public void DodajSkojarzenie(IPara<T> skojarzenie)
        {
            if (skojarzenie == null) throw new ArgumentNullException("Skojarzenie jest puste!");
            Skojarzenia.Add(skojarzenie);
        }

        /// <summary>
        /// Funkcja dodająca cały zbiór skojarzeń do grafu. 
        /// </summary>
        /// <param name="skojarzenia"></param>
        public void DodajSkojarzenie(IEnumerable<IPara<T>> skojarzenia)
        {
            if (skojarzenia == null) throw new ArgumentNullException("Skojarzenia są puste!");
            using (var iterator = skojarzenia.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    if (iterator.Current != null && !Skojarzenia.Contains(iterator.Current)) Skojarzenia.Add(iterator.Current);
                }
            }
        }

        /// <summary>
        /// Funkcja używana w przypadku grafu dwudzielnego. Dodaje wierzhołek do podzbioru wierzchołków V1.
        /// </summary>
        /// <param name="wierzcholek"></param>
        public void DodajDoZbioruV1(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzcholek juz istnieje w zbiorze V1");
            Zbior_V1.Add(wierzcholek);
        }

        /// <summary>
        /// Funkcja używana w przypadku grafu dwudzielnego. Dodaje cały zbiór wierzhołków do podzbioru wierzchołków V1.
        /// </summary>
        /// <param name="zbior_wierzcholkow"></param>
        public void DodajDoZbioruV1(IEnumerable<T> zbior_wierzcholkow)
        {
            if (zbior_wierzcholkow == null) throw new ArgumentNullException("Zbior wierzcholkow jest pusty!");
            using (var iterator = zbior_wierzcholkow.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    if (iterator.Current != null && !Zbior_V1.Contains(iterator.Current)) Zbior_V1.Add(iterator.Current);
                }
            }
        }

        /// <summary>
        /// Funkcja używana w przypadku grafu dwudzielnego. Dodaje wierzhołek do podzbioru wierzchołków V2.
        /// </summary>
        /// <param name="wierzcholek"></param>
        public void DodajDoZbioruV2(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzcholek juz istnieje w zbiorze V2");
            Zbior_V2.Add(wierzcholek);
        }

        /// <summary>
        /// Funkcja używana w przypadku grafu dwudzielnego. Dodaje cały zbiór wierzhołków do podzbioru wierzchołków V2.
        /// </summary>
        /// <param name="zbior_wierzcholkow"></param>
        public void DodajDoZbioruV2(IEnumerable<T> zbior_wierzcholkow)
        {
            if (zbior_wierzcholkow == null) throw new ArgumentNullException("Zbior wierzcholkow jest pusty!");
            using (var iterator = zbior_wierzcholkow.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    if (iterator.Current != null && !Zbior_V2.Contains(iterator.Current)) Zbior_V2.Add(iterator.Current);
                }
            }
        }

        /// <summary>
        /// Funkcja dodająca etykiete dla wierzchołka
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <param name="etykieta"></param>
        /// <returns>bool</returns>
        public bool DodajEtykiete(T wierzcholek, K etykieta)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Wierzholki.Contains(wierzcholek)) return false;
            Etykiety.Add(wierzcholek, etykieta);
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca etykiete danego wierzcholka
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>int</returns>
        public K ZwrocEtykiete(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty");
            return Etykiety[wierzcholek];
        }

        public bool UsunEtykiete(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty!");
            if (!Etykiety.ContainsKey(wierzcholek)) return false;
            Etykiety.Remove(wierzcholek);
            return true;
        }

        /// <summary>
        /// Funkcja do usuwania pojedyńczych wierzchołków z grafu.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>bool</returns>
        public bool UsunWierzholek(T wierzholek)
        {
            if (wierzholek == null) throw new ArgumentNullException();
            if (!Wierzholki.Contains(wierzholek)) return false;
            Wierzholki.Remove(wierzholek);
            return true;
        }

        /// <summary>
        /// Funkcja do zbiorowego usuwania wierzchołków.
        /// </summary>
        /// <param name="zbior_wierzholkow"></param>
        public void UsunWierzholek(IEnumerable<T> zbior_wierzholkow)
        {
            if (zbior_wierzholkow == null) throw new ArgumentNullException();
            using (var iterator = zbior_wierzholkow.GetEnumerator())
            {
                while (iterator.MoveNext())
                {
                    if(iterator.Current != null)
                    {
                        Wierzholki.Remove(iterator.Current);
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja do flagowania odwiedzonych wierzchołków w trakcie przechodzenia po grafie.
        /// </summary>
        /// <param name="v1"></param>
        /// <returns>bool</returns>
        public bool OdwiedzamWierzholek(T v1)
        {
            if (v1 == null) throw new ArgumentNullException();
            if (!Wierzholki.Contains(v1)) return false;
            OdwiedzoneWierzholki[v1] = true;
            return true;
        }

        /// <summary>
        /// Funkcja do sprawdzenia skojarzenia między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>true or false</returns>
        public bool IsSkojarzony(T v1, T v2)
        {
            if (v1 == null || v2 == null) throw new ArgumentNullException("Jeden z wierzchołków jest pusty");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) return false;
            IPara<T> para = new Para<T>(v1, v2);
            return Skojarzenia.Contains(para);
        }

        public bool IsSkojarzony(T wierzcholek)
        {
            //if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek) || wierzcholek == null) return false;
            foreach (var skojarzenie in Skojarzenia)
            {
                if (skojarzenie.Zawiera(wierzcholek)) return true;
            }
            return false;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wolnych wierzchołków podzbioru V1.
        /// </summary>
        /// <returns>List of objects from type T</returns>
        public IEnumerable<T> ZwrocZbiorWolnychV1()
        {
            IEnumerable<T> result = new List<T>();
            foreach (var wierzcholek in Zbior_V1)
            {
                int licznik = 0;  // ---- zabezpieczenie - aby sprawdził najpierw wszystkie krawędzie zanim zwróci 
                foreach (var para in Skojarzenia)
                {
                    if (para.Zawiera(wierzcholek)) licznik++;
                }
                if (licznik == 0) yield return wierzcholek;
            }
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wolnych wierzchołków podzbioru V2.
        /// </summary>
        /// <returns>List of objects from type T</returns>
        public IEnumerable<T> ZwrocZbiorWolnychV2()
        {
            foreach (var wierzcholek in Zbior_V2)
            {
                int licznik = 0;
                foreach (var para in Skojarzenia)
                {
                    if (para.Zawiera(wierzcholek)) licznik++;
                }
                if (licznik == 0) yield return wierzcholek;
            }
        }

        /// <summary>
        /// Funkcja zwracająca zbiór nieskojarzonych krawędzi grafu.
        /// </summary>
        /// <returns>List of objects from type Para<T></returns>
        public IEnumerable<IPara<T>> ZwrocNieskojarzoneKrawedzie()
        {
            foreach (var krawedz in Krawedzie)
            {
                if (!Skojarzenia.Contains(krawedz))
                {
                    if (Zbior_V1.Contains(krawedz.ZwrocPierwszy()) && Zbior_V2.Contains(krawedz.ZwrocDrugi())) yield return krawedz;
                    else  // jeśli pierwszy i drugi wierzchołek z krawędzi nie są odpowiednio ustawione to następnie zwracamy krawędź gdzie te dwa wierzchołki znajdują się w odpowiednim miejscu
                    {
                        yield return new Para<T>(krawedz.ZwrocDrugi(), krawedz.ZwrocPierwszy());
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja do skopiowania skojarzeń oraz podzbiorów wierzchołków V1 i V2 z jednego grafu do drugiego.
        /// </summary>
        /// <param name="graf"></param>
        public void KopiujSkojarzeniaZbiory(IGraf<T, K> graf)
        {
            if (graf == null) throw new ArgumentNullException("Graf jest pusty!");
            Skojarzenia = graf.ZwrocSkojarzenia().ToList();
            Zbior_V1 = graf.ZwrocZbiorV1().ToList();
            Zbior_V2 = graf.ZwrocZbiorV2().ToList();
        }

        /// <summary>
        /// Funkcja wykonująca sumę prostą ze zbioru skojarzeń oraz ze zbioru krawędzi ścieżki powiększającej.
        /// </summary>
        /// <param name="sciezka_powiekszajaca"></param>
        public void SumaProsta(IList<IPara<T>> sciezka_powiekszajaca)
        {
            IList<IPara<T>> suma = SumaZbiorow(sciezka_powiekszajaca).ToList();
            IList<IPara<T>> iloczyn = IloczynZbiorow(sciezka_powiekszajaca).ToList();
            foreach (var para in iloczyn)
            {
                // --- usuwanie cześci wspólnej skojarzeń. Krawędź skojarzona staje się nieskojarzoną -> zmiana kierunku krawędzi (od V1 do V2)
                UsunKrawedz(para);
                suma.Remove(para);
                para.Zamiana();
                DodajKrawedz(para);
            }
            foreach (var para in suma)
            {
                // --- jeśli skojarzenia są źle skierowane (z zbioru v1 w kierunku v2) to należy zamienić kierunek (skojarzenia idą z V2 do V1)
                if (Zbior_V1.Contains(para.ZwrocPierwszy()) && Zbior_V2.Contains(para.ZwrocDrugi()))
                {
                    UsunKrawedz(para); 
                    para.Zamiana();
                    DodajKrawedz(para);
                }
            }
            //--- wynik naszej sumy prostej należy skopiować do zbioru skojarzeń
            Skojarzenia = suma.ToList();
        }

        /// <summary>
        /// Funkcja wykonująca sumę zbiorów skojarzeń oraz krawędzi ścieżki powiększającej.
        /// </summary>
        /// <param name="sciezka_powiekszajaca"></param>
        /// <returns>List of objects from type Para<T></returns>
        public IEnumerable<IPara<T>> SumaZbiorow(IList<IPara<T>> sciezka_powiekszajaca)
        {
            if (sciezka_powiekszajaca == null) throw new ArgumentNullException("Sciezka jest pusta!");
            IList<IPara<T>> wynik = ZwrocSkojarzenia().ToList();
            foreach (var para in sciezka_powiekszajaca)
            {
                if (!wynik.Contains(para)) wynik.Add(para);
            }
            return wynik;
        }

        /// <summary>
        /// Funkcja wykonująca iloczyn zbiorów skojarzeń oraz krawędzi ścieżki powiększającej.
        /// </summary>
        /// <param name="sciezka_powiekszajaca"></param>
        /// <returns>List of objects from type Para<T></returns>
        public IEnumerable<IPara<T>> IloczynZbiorow(IList<IPara<T>> sciezka_powiekszajaca)
        {
            if (sciezka_powiekszajaca == null) throw new ArgumentNullException("Sciezka jest pusta!");
            IList<IPara<T>> wynik = new List<IPara<T>>();
            foreach (var para in sciezka_powiekszajaca)
            {
                if (ZwrocSkojarzenia().Contains(para)) wynik.Add(para);
            }
            return wynik;
        }

        /// <summary>
        /// Funkcja zwarająca zbiór wierzchołków należących do podzbioru V1.
        /// </summary>
        /// <returns>List of objects from type T</returns>
        public IEnumerable<T> ZwrocZbiorV1()
        {
            return Zbior_V1;
        }

        /// <summary>
        /// Funkcja zwarająca zbiór wierzchołków należących do podzbioru V2.
        /// </summary>
        /// <returns>List of objects from type T</returns>
        public IEnumerable<T> ZwrocZbiorV2()
        {
            return Zbior_V2;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wszystkich krawędzi grafu.
        /// </summary>
        /// <returns>List of objects from type Para<T></returns>
        public IEnumerable<IPara<T>> ZwrocKrawedzie()
        {
            return Krawedzie;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wszystkich wierzchołków grafu.
        /// </summary>
        /// <returns>List of objects from type T</returns>
        public IEnumerable<T> ZwrocWierzholki()
        {
            return Wierzholki;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wszystkich skojarzeń grafu.
        /// </summary>
        /// <returns>List of objects from type Para<T></returns>
        public IEnumerable<IPara<T>> ZwrocSkojarzenia()
        {
            foreach (var para in Skojarzenia)
            {
                if (Zbior_V2.Contains(para.ZwrocPierwszy()) && Zbior_V1.Contains(para.ZwrocDrugi())) yield return para;
                else yield return new Para<T>(para.ZwrocDrugi(), para.ZwrocPierwszy());
            }
        }
        
        /// <summary>
        /// Funkcja zwracająca skojarzenie danego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public IPara<T> ZwrocSkojarzenie(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzcholek nie istnieje!");
            if (!IsSkojarzony(wierzcholek)) return null;
            return Skojarzenia.Select(x => x).Where(x => x.Zawiera(wierzcholek)).First();
        }

        /// <summary>
        /// Funkcja krawędź o największej wadze.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>Object from Para<T> type</returns>
        public abstract IEnumerable<IPara<T>> ZwrocKrawedzRownejWarunkowi(T wierzcholek);

        /// <summary>
        /// Funkcja usuwająca krawędź w grafie.
        /// </summary>
        /// <param name="para"></param>
        /// <returns>bool</returns>
        public bool UsunKrawedz(IPara<T> para)
        {
            if (para == null) throw new ArgumentNullException("Krawedz jest pusta");
            Krawedzie.Remove(para);
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi w grafie.
        /// </summary>
        /// <returns>int</returns>
        public int IloscKrawedzi()
        {
            return Krawedzie.Count;
        }

        /// <summary>
        /// Funkcja zwracająca ilość wierzchołków w grafie.
        /// </summary>
        /// <returns>int</returns>
        public int IloscWierzholkow()
        {
            return Wierzholki.Count;
        }

        /// <summary>
        /// Funkcja dodająca krawędź do grafu.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="waga"></param>
        /// <returns>bool</returns>
        public abstract bool DodajKrawedz(T v1, T v2, K waga);

        /// <summary>
        /// Funkcja zwracająca stopień grafu.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>int</returns>
        public abstract int Stopien(T wierzholek);

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi wychodzących z wierzchołków.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>int</returns>
        public abstract int Wejscia(T wierzholek);

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi przychodzących z wierzchołków.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>int</returns>
        public abstract int Wyjscia(T wierzholek);    

        /// <summary>
        /// Funkcja sprawdzająca czy przekazywane wierzchołki są sąsiadami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public abstract bool IsSasiad(T v1, T v2);

        /// <summary>
        /// Funkcja sprawdzająca czy krawędź między przekazywanymi wierzchołkami jest odwiedzona.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public abstract bool IsOdwiedzone(T v1, T v2);

        /// <summary>
        /// Funkcja odpowiadająca za flagowanie odwiedzonych krawędzi w trakcie przechodzenia po grafie.
        /// </summary>
        /// <param name="krawedz"></param>
        /// <returns>bool</returns>
        public abstract bool OdwiedzamKrawedz(IPara<T> krawedz);

        /// <summary>
        /// Funkcja usuwająca krawędź między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public abstract bool UsunKrawedz(T v1, T v2);

        /// <summary>
        /// Funkcja zwracająca wagę krawędzi między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>Value of type K</returns>
        public abstract K ZwrocWage(T v1, T v2);

        /// <summary>
        /// Funkcja zwracająca wagę krawędzi.
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public abstract K ZwrocWage(IPara<T> para);

        /// <summary>
        /// Funkcja zwracająca ze zbioru krawędź między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>Object of type Para<T></returns>
        public abstract IPara<T> ZwrocKrawedz(T v1, T v2);

        /// <summary>
        /// Funkcja zwracająca zbiór wierzchołków sąsiadujących z danym wierzchołkiem.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>List of values from type T</returns>
        public abstract IEnumerable<T> ZwrocSasiadow(T wierzholek);

        /// <summary>
        /// Funkcja zwracająca zbiór sąsiadów z nieodwiedzonych krawędzi danego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>List of values from type T</returns>
        public abstract IEnumerable<T> ZwrocSasiadowNieodwiedzonychKrawedzi(T wierzcholek);

        /// <summary>
        /// Funkcja zwracająca zbiór nieodwiedzonych sąsiadów danego wierzchołka.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>List of values from type T</returns>
        public abstract IEnumerable<T> ZwrocNieodwiedzonychSasiadow(T wierzholek);

        /// <summary>
        /// Funkcja zwracająca krawędzie połączone z danym wierzchołkiem.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public abstract IEnumerable<IPara<T>> ZwrocKrawedzieWierzcholka(T wierzcholek);

        /// <summary>
        /// Funkcja usuwająca wszysktie elementy listy skojarzeń.
        /// </summary>
        public void UsunSkojarzenia()
        {
            Skojarzenia.Clear();
        }

        /// <summary>
        /// Funkcja zwracająca wszystkie wagi w grafie.
        /// </summary>
        /// <returns></returns>
        public Dictionary<IPara<T>, K> ZwrocWagi()
        {
            return Wagi;
        }

        /// <summary>
        /// Funkcja kopiująca wagi z jednego grafu do drugiego.
        /// </summary>
        /// <param name="graf"></param>
        public void KopiujWagi(IGraf<T, K> graf)
        {
            Wagi = graf.ZwrocWagi();
        }

        /// <summary>
        /// Funkcja zwracająca wagi krawędzi skojarzonych.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<K> ZwrocWagiSkojarzen()
        {
            foreach (var skojarzenie in Skojarzenia)
            {
                skojarzenie.Zamiana();
                if (Wagi.ContainsKey(skojarzenie)) yield return Wagi[skojarzenie];
            }
        }

        /// <summary>
        /// Funkcja zwracająca "pierwszy" wierzchołek grafu. Zwykle to wierzchołek w zerowym indeksie grafu.
        /// </summary>
        /// <returns></returns>
        public T ZwrocPierwszyWierzcholek()
        {
            if (Wierzholki.Count == 0) throw new ArgumentException("Zbirór wierzchołków jest pusty");
            return Wierzholki[0];
        }

        /// <summary>
        /// Funkcja przypisująca kolor wierzchołkowi.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <param name="kolor"></param>
        /// <returns></returns>
        public bool DodajKolor(T wierzcholek, K kolor)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            Kolory[wierzcholek] = kolor;
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca kolor danego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public K ZwrocKolor(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Kolory.ContainsKey(wierzcholek)) return default(K);
            return Kolory[wierzcholek];
        }

        /// <summary>
        /// Funkcja ustawiająca wierzchołki w kolejności nierosnących stopni.
        /// </summary>
        public void UstawWierzcholkiLF()
        {
            SortedList<T, int> kolekcja_LF = new SortedList<T, int>();
            foreach (var wierzcholek in ZwrocWierzholki())
            {
                var ilosc_sasiadow = ZwrocSasiadow(wierzcholek).Count();
                kolekcja_LF.Add(wierzcholek, ilosc_sasiadow);
            }
            Wierzholki = kolekcja_LF.OrderByDescending(x => x.Value).Select(x => x.Key).ToList();
        }

        /// <summary>
        /// Funkcja zwracająca wszystkie kolory sąsiadów.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public IEnumerable<K> ZwrocKolorySasiadowWierzcholka(T wierzcholek)
        {
            foreach (var sasiad in ZwrocSasiadow(wierzcholek))
            {
                if(Kolory.ContainsKey(sasiad))
                    yield return Kolory[sasiad];
            }
        }

        /// <summary>
        /// Funkcja zwracająca maskrymalny numer koloru danego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public K ZwrocMaksymalnyKolorWierzcholka(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty");
            return Maksymalny_Kolor_Wierzcholka[wierzcholek];
        }

        /// <summary>
        /// Funkcja przydzielająca maksymalny numer koloru wierzchołkowi.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <param name="kolor"></param>
        /// <returns></returns>
        public bool DodajMaksymalnyKolor(T wierzcholek, K kolor)
        {
            if (wierzcholek == null || kolor == null) throw new ArgumentNullException("Parametr(y) są puste");
            Maksymalny_Kolor_Wierzcholka[wierzcholek] = kolor;
            return true;
        }

        /// <summary>
        /// Funkcja usuwająca kolor ze zbioru dopuszczalnych danego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <param name="kolor"></param>
        /// <returns></returns>
        public bool UsunKolor(T wierzcholek, K kolor)
        {
            if (wierzcholek == null || kolor == null) throw new ArgumentNullException("Parametr(y) są puste");
            if (!Dopuszalne_Kolory.ContainsKey(wierzcholek)) return false;
            Dopuszalne_Kolory[wierzcholek].Remove(kolor);
            return true;
        }
        
        /// <summary>
        /// Funkcja dodająca zbiór dopuszczalnych kolorów do indeksu wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <param name="kolory_dopuszczalne"></param>
        /// <returns></returns>
        public bool DodajDopuszczalneKolory(T wierzcholek, IList<K> kolory_dopuszczalne)
        {
            if (wierzcholek == null || kolory_dopuszczalne.Count == 0) throw new ArgumentNullException("Parametr(y) są puste");
            Dopuszalne_Kolory[wierzcholek] = kolory_dopuszczalne.ToList();
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór dopuszczalnych kolorów danego wierzchołka, pod warunkiem że zawiera jakieś kolory
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public IList<K> ZwrocZbiorKolorówDopuszczalnych(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty");
            try
            {
                return Dopuszalne_Kolory[wierzcholek];
            }
            catch
            {
                return new List<K>();
            }
        }
    }
}