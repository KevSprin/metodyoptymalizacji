﻿using System.Collections.Generic;

namespace BibliotekaGraf
{
    public interface IGraf<T, K>
    {
        bool DodajWierzholek(T wierzholek);

        void DodajWierzholek(IEnumerable<T> zbior_wierzholkow);

        bool DodajKrawedz(T v1, T v2, K waga);

        bool DodajKrawedz(IPara<T> krawedz);

        void DodajSkojarzenie(T v1, T v2);

        void DodajSkojarzenie(IPara<T> skojarzenie);

        void DodajSkojarzenie(IEnumerable<IPara<T>> skojarzenia);

        void DodajDoZbioruV1(T wierzcholek);

        void DodajDoZbioruV1(IEnumerable<T> zbior_wierzcholkow);

        void DodajDoZbioruV2(T wierzcholek);

        void DodajDoZbioruV2(IEnumerable<T> zbior_wierzcholkow);

        bool DodajEtykiete(T wierzcholek, K etykieta);

        bool DodajKolor(T wierzcholek, K kolor);

        bool DodajMaksymalnyKolor(T wierzcholek, K kolor);

        bool DodajDopuszczalneKolory(T wierzcholek, IList<K> kolory_dopuszczalne);

        bool UsunWierzholek(T wierzholek);

        void UsunWierzholek(IEnumerable<T> zbior_wierzholkow);

        bool UsunKrawedz(T v1, T v2);

        bool UsunKrawedz(IPara<T> para);

        bool UsunEtykiete(T wierzcholek);

        bool UsunKolor(T wierzcholek, K kolor);

        void UsunSkojarzenia();

        bool IsSasiad(T v1, T v2);

        int Stopien(T wierzholek);

        int Wyjscia(T wierzholek);

        int Wejscia(T wierzholek);

        int IloscWierzholkow();

        int IloscKrawedzi();

        bool IsSkojarzony(T v1, T v2);

        bool IsSkojarzony(T wierzcholek);

        void KopiujSkojarzeniaZbiory(IGraf<T, K> graf);

        bool IsOdwiedzone(T v1, T v2);

        bool OdwiedzamKrawedz(IPara<T> krawedz);

        bool OdwiedzamWierzholek(T v1);

        void SumaProsta(IList<IPara<T>> sciezka_powiekszajaca);

        IEnumerable<IPara<T>> SumaZbiorow(IList<IPara<T>> sciezka_powiekszajaca);

        IEnumerable<IPara<T>> IloczynZbiorow(IList<IPara<T>> sciezka_powiekszajaca);

        K ZwrocWage(T v1, T v2);

        K ZwrocWage(IPara<T> para);

        K ZwrocEtykiete(T wierzcholek);

        K ZwrocKolor(T wierzcholek);

        IEnumerable<T> ZwrocSasiadow(T wierzholek);

        IEnumerable<T> ZwrocNieodwiedzonychSasiadow(T wierzholek);

        T ZwrocPierwszyWierzcholek();

        IEnumerable<T> ZwrocWierzholki();

        IEnumerable<IPara<T>> ZwrocKrawedzie();

        IEnumerable<IPara<T>> ZwrocKrawedzieWierzcholka(T wierzcholek);

        IEnumerable<IPara<T>> ZwrocKrawedzRownejWarunkowi(T wierzcholek);

        IEnumerable<T> ZwrocSasiadowNieodwiedzonychKrawedzi(T v1);

        IEnumerable<K> ZwrocKolorySasiadowWierzcholka(T wierzcholek);

        IList<K> ZwrocZbiorKolorówDopuszczalnych(T wierzcholek);

        K ZwrocMaksymalnyKolorWierzcholka(T wierzcholek);

        IPara<T> ZwrocKrawedz(T v1, T v2);

        IEnumerable<T> ZwrocZbiorV1();

        IEnumerable<T> ZwrocZbiorV2();

        IEnumerable<IPara<T>> ZwrocSkojarzenia();

        IPara<T> ZwrocSkojarzenie(T wierzcholek);

        IEnumerable<IPara<T>> ZwrocNieskojarzoneKrawedzie();

        Dictionary<IPara<T>, K> ZwrocWagi();

        void KopiujWagi(IGraf<T, K> graf);

        IEnumerable<K> ZwrocWagiSkojarzen();

        void UstawWierzcholkiLF();
    }

    public interface IPara<T>
    {
        T ZwrocPierwszy();
        T ZwrocDrugi();
        IPara<T> ZwrocOdwroconaPare();
        bool Zawiera(T wartosc);
        void Zamiana();
        IPara<T> Clone();
    }
}
