﻿using System;
using System.Collections.Generic;

namespace BibliotekaGraf
{
    public class GrafSkierowany<T, K> : GrafAbstrakcyjny<T, K>
    {
        /// <summary>
        /// Funkcja tworząca krawędź między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="waga"></param>
        /// <returns>bool</returns>
        public override bool DodajKrawedz(T v1, T v2, K waga)
        {
            if (v1 == null || v2 == null || waga == null) throw new ArgumentNullException("Jeden z argumentów jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) return false;
            IPara<T> para = new Para<T>(v1, v2);
            if (Krawedzie.Contains(para)) return false;
            Krawedzie.Add(para);
            Wagi[para] = waga;
            OdwiedzoneKrawedzie[para] = false;
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca wagę krawędzi między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>Value from type K</returns>
        public override K ZwrocWage(T v1, T v2)
        {
            if (v1 == null || v2 == null) throw new ArgumentNullException("Jeden z wierzchołków jest pusty!");
            IPara<T> para = new Para<T>(v1, v2);
            if (!Wagi.ContainsKey(para)) throw new ArgumentException("Taka krawiędź nie istnieje!");
            return Wagi[para];
        }

        /// <summary>
        /// Funkcja zwracająca wagę krawędzi.
        /// </summary>
        /// <param name="para"></param>
        /// <returns>Value from type K</returns>
        public override K ZwrocWage(IPara<T> para)
        {
            if (para == null) throw new ArgumentNullException("Krawędź jest pusta");
            if (!Wagi.ContainsKey(para)) throw new ArgumentException("Taka krawędź nie istnieje!");
            return Wagi[para];
        }

        /// <summary>
        /// Funkcja usuwająca krawędź między dwoma wierzchołkami w grafie.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public override bool UsunKrawedz(T v1, T v2)
        {
            if (v1 == null || v2 == null) throw new ArgumentNullException("Jeden z wierzchołków jest pusty!");
            IPara<T> para = new Para<T>(v1, v2);
            if (Krawedzie.Contains(para))
            {
                Krawedzie.Remove(para);
                Wagi.Remove(para);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Funkcja sprawdzająca czy przekazywane wierzchołki są sąsiadami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public override bool IsSasiad(T v1, T v2)
        {
            if (v1 == null || v2 == null) throw new ArgumentNullException("Jeden z wierzchołków jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) throw new ArgumentException("Jeden z wierzchołków nie jest zawarty w grafie");
            return Krawedzie.Contains(new Para<T>(v1, v2));
        }

        /// <summary>
        /// Funkcja zwracająca stopień grafu.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>int</returns>
        public override int Stopien(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie!");
            return Wejscia(wierzcholek) + Wyjscia(wierzcholek);
        }

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi przychodzących z wierzchołków.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>int</returns>
        public override int Wyjscia(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie!");
            int licznik = 0;
            foreach (var para in Krawedzie)
            {
                if (para.ZwrocPierwszy().Equals(wierzcholek)) licznik++;
            }
            return licznik;
        }

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi wychodzących z wierzchołków.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>int</returns>
        public override int Wejscia(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie!");
            int licznik = 0;
            foreach (var para in Krawedzie)
            {
                if (para.ZwrocDrugi().Equals(wierzcholek)) licznik++;
            }
            return licznik;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wierzchołków sąsiadujących z danym wierzchołkiem.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>List of values from type T</returns>
        public override IEnumerable<T> ZwrocSasiadow(T wierzcholek)
        {
            foreach (IPara<T> para in Krawedzie)
            {
                if (para.ZwrocPierwszy().Equals(wierzcholek))
                    yield return para.ZwrocDrugi();
            }
        }

        /// <summary>
        /// Funkcja sprawdzająca czy krawędź między przekazywanymi wierzchołkami jest odwiedzona.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public override bool IsOdwiedzone(T v1, T v2)
        {
            if (v1 == null || v2 == null) throw new ArgumentNullException("Jeden z wierzchołków jest pusty");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) throw new ArgumentException("Conajmniej jeden z wierzchołków nie jest zawarty w grafie");
            IPara<T> para = new Para<T>(v1, v2);
            return OdwiedzoneKrawedzie[para];
        }

        /// <summary>
        /// Funkcja odpowiadająca za flagowanie odwiedzonych krawędzi w trakcie przechodzenia po grafie.
        /// </summary>
        /// <param name="krawedz"></param>
        /// <returns>bool</returns>
        public override bool OdwiedzamKrawedz(IPara<T> krawedz)
        {
            if (krawedz == null) throw new ArgumentNullException("Krawędź jest pusta");
            if (!Krawedzie.Contains(krawedz)) return false;
            OdwiedzoneKrawedzie[krawedz] = true;
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca zbiór nieodwiedzonych sąsiadów danego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns>List of values from type T</returns>
        public override IEnumerable<T> ZwrocNieodwiedzonychSasiadow(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie");
            var sasiedzi = ZwrocSasiadow(wierzcholek);
            foreach (var sasiad in sasiedzi)
            {
                if (!OdwiedzoneWierzholki[sasiad]) yield return sasiad;
            }
        }

        /// <summary>
        /// Funkcja zwracająca zbiór sąsiadów z nieodwiedzonych krawędzi danego wierzchołka.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>List of values from type T</returns>
        public override IEnumerable<T> ZwrocSasiadowNieodwiedzonychKrawedzi(T wierzholek)
        {
            // --- zabezbieczenie
            if (wierzholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Wierzholki.Contains(wierzholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie: " + wierzholek);
            foreach (var para in OdwiedzoneKrawedzie.Keys)
            {
                if (!OdwiedzoneKrawedzie[para])
                {
                    if (para.ZwrocPierwszy().Equals(wierzholek))
                        yield return para.ZwrocDrugi();
                }
            }
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wszystkich krawędzi grafu.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public override IPara<T> ZwrocKrawedz(T v1, T v2)
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null) throw new ArgumentNullException("Conajmniej jeden wierzchołek jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) throw new ArgumentException("Conajmniej jeden z wierzchołków nie jest zawarty w grafie!");
            IPara<T> para = new Para<T>(v1, v2);
            if (Krawedzie.Contains(para)) return Krawedzie.Find(x => x.Equals(para));
            throw new ArgumentException("Krawędź nie istnieje w tym grafie!");
        }

        /// <summary>
        /// Funkcja zwracająca zbiór krawędzi połączonych z danym wierzchołkiem.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public override IEnumerable<IPara<T>> ZwrocKrawedzieWierzcholka(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzchołek nie istnieje w grafie!");
            foreach (var krawedz in Krawedzie)
            {
                if (krawedz.ZwrocPierwszy().Equals(wierzcholek)) yield return krawedz;
            }
        }

        /// <summary>
        /// Funkcja zwracająca wszystkie krawędzie spełniający warunek l(x) + l(y) = W(x,y)
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <returns></returns>
        public override IEnumerable<IPara<T>> ZwrocKrawedzRownejWarunkowi(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty!");
            if (!Zbior_V1.Contains(wierzcholek)) throw new ArgumentException("Wierzcholek nie nalezy do zbioru V1");
            Dictionary<IPara<T>, K> results = new Dictionary<IPara<T>, K>();
            foreach (var sasiad in ZwrocSasiadow(wierzcholek))
            {
                var etykieta_wierzcholka = Convert.ToInt32(Etykiety[wierzcholek]);
                var etykieta_sasiada = Convert.ToInt32(Etykiety[sasiad]);
                var suma_etykiet = etykieta_sasiada + etykieta_wierzcholka;
                IPara<T> krawedz = ZwrocKrawedz(wierzcholek, sasiad);
                if (Wagi[krawedz].Equals(suma_etykiet)) yield return krawedz;
            }
        }
    }
}
