﻿using System;
using System.Collections.Generic;

namespace BibliotekaGraf
{
    public class GrafNieskierowany<T, K> : GrafAbstrakcyjny<T, K>
    {
        /// <summary>
        /// Funkcja odpowiadająca za flagowanie odwiedzonych krawędzi w trakcie przechodzenia po grafie.
        /// </summary>
        /// <param name="krawedz"></param>
        /// <returns>bool</returns>
        public override bool OdwiedzamKrawedz(IPara<T> krawedz)
        {
            // --- zabezbieczenie
            if (krawedz == null) throw new ArgumentNullException("Argument jest pusty!");
            if (!Krawedzie.Contains(krawedz)) return false;
            OdwiedzoneKrawedzie[krawedz] = true;
            OdwiedzoneWierzholki[krawedz.ZwrocDrugi()] = true;
            OdwiedzoneWierzholki[krawedz.ZwrocPierwszy()] = true;
            return true;
        }

        /// <summary>
        /// Funkcja tworząca krawędź między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="waga"></param>
        /// <returns>bool</returns>
        public override bool DodajKrawedz(T v1, T v2, K waga)
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null || waga == null) throw new ArgumentNullException("Jeden lub więcej z argumentów są puste!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) return false;
            IPara<T> para = new Para<T>(v1, v2);
            if (Krawedzie.Contains(para)) return false;
            Krawedzie.Add(para);
            Wagi[para] = waga;
            OdwiedzoneKrawedzie[para] = false;
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca ze zbioru krawędź między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>Object of type Para<T></returns>
        public override IPara<T> ZwrocKrawedz(T v1, T v2) // metoda zwracająca 
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null) throw new ArgumentNullException("Conajmniej jeden wierzchołek jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) throw new ArgumentException("Conajmniej jeden z wierzchołków nie jest zawarty w grafie!");
            IPara<T> para_1 = new Para<T>(v1, v2), para_2 = new Para<T>(v2, v1);
            if (Krawedzie.Contains(para_1)) return Krawedzie.Find(x => x.Equals(para_1));
            else return Krawedzie.Find(x => x.Equals(para_2));
        }

        /// <summary>
        /// Funkcja sprawdzająca czy krawędź między przekazywanymi wierzchołkami jest odwiedzona.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public override bool IsOdwiedzone(T v1, T v2) // metoda do sprawdzenia czy krawędź między dwoma konkretnymi wierzhołkami jest już odwiedzona
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null ) throw new ArgumentNullException("Conajmniej jeden wierzchołek jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) return false;
            IPara<T> para = new Para<T>(v1, v2);
            return OdwiedzoneKrawedzie[para];            
        }

        /// <summary>
        /// Funkcja zwracająca wagę krawędzi między dwoma wierzchołkami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>Value from type K</returns>
        public override K ZwrocWage(T v1, T v2)
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null) throw new ArgumentNullException("Conajmniej jeden wierzchołek jest pusty!");
            IPara<T> para = new Para<T>(v1, v2);
            if (!Wagi.ContainsKey(para)) para.Zamiana();
            return Wagi[para];
        }

        /// <summary>
        /// Funkcja zwracająca wagę krawędzi.
        /// </summary>
        /// <param name="para"></param>
        /// <returns>Value from type K</returns>
        public override K ZwrocWage(IPara<T> para)
        {
            if (para == null) throw new ArgumentNullException("Krawędź jest pusta!");
            if (!Wagi.ContainsKey(para) && !Wagi.ContainsKey(para.ZwrocOdwroconaPare())) throw new ArgumentException("Krawędź nie istnieje!");
            return Wagi[para];
        }

        /// <summary>
        /// Funkcja wykonująca sumę prostą ze zbioru skojarzeń oraz ze zbioru krawędzi ścieżki powiększającej.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public override bool UsunKrawedz(T v1, T v2)
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null) throw new ArgumentNullException("Conajmniej jeden wierzchołek jest pusty!");
            IPara<T> para = new Para<T>(v1, v2);
            if (!Krawedzie.Contains(para)) return false;

            Krawedzie.Remove(para); // usuwanie krawedzi z zbioru krawedzi
            Wagi.Remove(para);      // usuwanie krawedzi z zbioru wag 
            OdwiedzoneKrawedzie.Remove(para); // usuwanie krawedzi z zbioru odwiedzonych krawedzi
            Skojarzenia.Remove(para); // usuwanie krawędzi z zbioru skojarzeń (jeśli nie ma krawędzi to skojarzenie nie jest możliwe);
            return true;
        }

        /// <summary>
        /// Funkcja sprawdzająca czy przekazywane wierzchołki są sąsiadami.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns>bool</returns>
        public override bool IsSasiad(T v1, T v2)
        {
            // --- zabezbieczenie
            if (v1 == null || v2 == null) throw new ArgumentNullException("Conajmniej jeden wierzchołek jest pusty!");
            if (!Wierzholki.Contains(v1) || !Wierzholki.Contains(v2)) throw new ArgumentException("Conajmniej jeden z wierzchołków nie jest zawarty w grafie!");
            return Krawedzie.Contains(new Para<T>(v1, v2));
        }

        /// <summary>
        /// Funkcja zwracająca zbiór wierzchołków sąsiadujących z danym wierzchołkiem.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>List of values from type T</returns>
        public override IEnumerable<T> ZwrocSasiadow(T wierzholek) // metoda zwracająca wszystkich sąsiadów
        {
            // --- zabezbieczenie
            if (wierzholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Wierzholki.Contains(wierzholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie: " + wierzholek);
            foreach (IPara<T> para in Krawedzie)
            {
                if (para.ZwrocPierwszy().Equals(wierzholek))
                    yield return para.ZwrocDrugi();
                else if (para.ZwrocDrugi().Equals(wierzholek))
                    yield return para.ZwrocPierwszy();
            }
        }

        /// <summary>
        /// Funkcja zwracająca zbiór nieodwiedzonych sąsiadów danego wierzchołka.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>List of values from type T</returns>
        public override IEnumerable<T> ZwrocNieodwiedzonychSasiadow(T wierzholek) // metoda zwracająca wyłącznie nieodwiedzonych sąsiadów
        {
            // --- zabezbieczenie
            if (wierzholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Wierzholki.Contains(wierzholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie: " + wierzholek);
            var sasiedzi = ZwrocSasiadow(wierzholek);
            foreach (var sasiad in sasiedzi)
            {
                if (!OdwiedzoneWierzholki[sasiad]) yield return sasiad;
            }
        }

        /// <summary>
        /// Funkcja zwracająca zbiór sąsiadów z nieodwiedzonych krawędzi danego wierzchołka.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>List of values from type T</returns>
        public override IEnumerable<T> ZwrocSasiadowNieodwiedzonychKrawedzi(T wierzholek) // metoda zwracająca Sąsiadów z niedowiedzonych krawędzi
        {
            // --- zabezbieczenie
            if (wierzholek == null) throw new ArgumentNullException("Wierzchołek jest pusty");
            if (!Wierzholki.Contains(wierzholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie: " + wierzholek);
            foreach (var para in OdwiedzoneKrawedzie.Keys)
            {
                if (!OdwiedzoneKrawedzie[para])
                {
                    if (para.ZwrocPierwszy().Equals(wierzholek))
                        yield return para.ZwrocDrugi();
                    else if (para.ZwrocDrugi().Equals(wierzholek))
                        yield return para.ZwrocPierwszy();
                }
            }
        }

        /// <summary>
        /// Funkcja zwracająca stopień grafu.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>int</returns>
        public override int Stopien(T wierzholek) //---- Obliczenie stopnia parzystości w grafie
        {
            // --- zabezbieczenie
            if (wierzholek == null) throw new ArgumentNullException("Wierzchołek jest pusty!");
            if (!Wierzholki.Contains(wierzholek)) throw new ArgumentException("Wierzchołek nie jest zawarty w grafie: " + wierzholek);
            int licznik = 0;
            foreach (var para in Krawedzie)
            {
                //if (para.ZwrocPierwszy().Equals(wierzholek)) licznik++;
                //if (para.ZwrocDrugi().Equals(wierzholek)) licznik++;
                if (para.Zawiera(wierzholek)) licznik++;
            }
            return licznik;
        }

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi wychodzących z wierzchołków.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>int</returns>
        public override int Wejscia(T wierzholek)
        {
            return Stopien(wierzholek); // przy grafie nieskierowanym wejscia i wyjscia sa to samo
        }

        /// <summary>
        /// Funkcja zwracająca ilość krawędzi przychodzących z wierzchołków.
        /// </summary>
        /// <param name="wierzholek"></param>
        /// <returns>int</returns>
        public override int Wyjscia(T wierzholek)
        {
            return Stopien(wierzholek); // dlatego dla jednego lub drugiego przypadku jest ten sam stopien obliczany
        }

        public override IEnumerable<IPara<T>> ZwrocKrawedzieWierzcholka(T wierzcholek)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzchołek jest pusty!");
            if (!Wierzholki.Contains(wierzcholek)) throw new ArgumentException("Wierzchołek nie istnieje w grafie!");
            foreach (var krawedz in Krawedzie)
            {
                if (krawedz.Zawiera(wierzcholek)) yield return krawedz;
            }
        }

        public override IEnumerable<IPara<T>> ZwrocKrawedzRownejWarunkowi(T wierzcholek)
        {
            throw new NotImplementedException();
        }
    }
}
