﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BibliotekaDrzewo
{
    public class Węzeł<T> : IWęzeł<T>, ICloneable
    {
        private IWęzeł<T> parent;
        private T wartość;
        private List<IWęzeł<T>> węzły = new List<IWęzeł<T>>();
        private int next_pointer = 0;

        /// <summary>
        /// Własność określająca czy dany węzeł został odwiedzony.
        /// </summary>
        public bool Visited { get; set; } = false;

        public Węzeł(T wartość)
        {
            this.wartość = wartość;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Funkcja dodająca węzeł rodzica do rozpatrywanego węzła
        /// </summary>
        /// <param name="Węzeł"></param>
        public void DodajParent(ref IWęzeł<T> Węzeł)
        {
            parent = Węzeł;
        }

        /// <summary>
        /// Funkcja dodająca węzeł sąsiadujący do rozpatrywanego węzła
        /// </summary>
        /// <param name="Węzeł"></param>
        public void DodajWęzeł(ref IWęzeł<T> Węzeł)
        {
            if (Węzeł == null) throw new ArgumentNullException("Węzeł is null");
            węzły.Add(Węzeł);
        }

        /// <summary>
        /// Funkcja zwracająca następny sąsiadujący węzeł.
        /// </summary>
        /// <returns></returns>
        public IWęzeł<T> ZwróćNastępny()
        {
            if (węzły.Count == 0) throw new ArgumentNullException("Węzeł nie ma następników");
            if (next_pointer >= węzły.Count) next_pointer = 0;
            var węzeł = węzły[next_pointer];
            next_pointer++;
            return węzeł;
        }

        /// <summary>
        /// Funkcja zwracająca wszystkie sąsiadujące węzły rozpatrywanego węzła.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IWęzeł<T>> ZwróćWęzły()
        {
            foreach (var węzeł in węzły)
            {
                yield return węzeł;
            }
        }

        /// <summary>
        /// Funkcja zwracająca zawartość naszego węzła.
        /// </summary>
        /// <returns></returns>
        public T ZwróćWartość()
        {
            return wartość;
        }

        /// <summary>
        /// Funkcja sprawdzająca czy rozpatrywany węzeł posiada sąsiadujące węzły
        /// </summary>
        /// <returns></returns>
        public bool MaWęzły()
        {
            return węzły.Count > 0;
        }

        public IWęzeł<T> ZwróćParent()
        {
            return parent;
        }
    }
}
