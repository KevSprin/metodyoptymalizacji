﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BibliotekaDrzewo
{
    public class Drzewo<T> : IDrzewo<T>, ICloneable
    {
        List<IWęzeł<T>> węzły = new List<IWęzeł<T>>();
        IWęzeł<T> korzeń;

        public Drzewo(T korzeń)
        {
            if (korzeń == null) throw new ArgumentNullException("Value is null");
            this.korzeń = new Węzeł<T>(korzeń);
            węzły.Add(this.korzeń);
        }

        IWęzeł<T> IDrzewo<T>.this[int index]
        {
            get { return węzły[index]; }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Funkcja podpinająca nowy węzeł pod już istniejący węzeł.
        /// </summary>
        /// <param name="parent_węzeł"></param>
        /// <param name="węzeł_wartość"></param>
        public void DodajWęzeł(IWęzeł<T> parent_węzeł, T węzeł_wartość)
        {
            if (węzeł_wartość == null) throw new ArgumentNullException("Value is null");
            if (!węzły.Contains(parent_węzeł)) throw new ArgumentException("Taki węzeł nie istnieje");
            IWęzeł<T> nowy_węzeł = new Węzeł<T>(węzeł_wartość);
            nowy_węzeł.DodajParent(ref parent_węzeł);           
            parent_węzeł.DodajWęzeł(ref nowy_węzeł);
            węzły.Add(nowy_węzeł);
        }

        /// <summary>
        /// Funkcja podpinająca nowy węzeł pod już istniejący węzeł.
        /// </summary>
        /// <param name="parent_wartość"></param>
        /// <param name="węzeł_wartość"></param>
        public void DodajWęzeł(T parent_wartość, T węzeł_wartość)
        {
            if (parent_wartość == null || węzeł_wartość == null) throw new ArgumentNullException("Values are null");
            var parent_węzeł = węzły.FirstOrDefault(x => x.ZwróćWartość().Equals(parent_wartość));
            if (parent_węzeł == null) throw new ArgumentNullException("Parent doesn't exist");
            IWęzeł<T> nowy_węzeł = new Węzeł<T>(węzeł_wartość);
            nowy_węzeł.DodajParent(ref parent_węzeł);
            parent_węzeł.DodajWęzeł(ref nowy_węzeł);
            węzły.Add(nowy_węzeł);
        }

        /// <summary>
        /// Funkcja sprawdzająca czy w rozpatrywanej gałęzi już istnieje dodawana wartość.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="nowy"></param>
        /// <returns></returns>
        public bool SprawdźParent(IWęzeł<T> parent, T nowy)
        {
            if (parent == null) return false;
            if (parent.ZwróćWartość().Equals(nowy)) return true;
            return SprawdźParent(parent.ZwróćParent(), nowy);
        }

        /// <summary>
        /// Funkcja usuwająca węzeł z drzewa
        /// </summary>
        /// <param name="węzeł"></param>
        public void UsuńWęzeł(IWęzeł<T> węzeł)
        {
            if (węzeł == null) throw new ArgumentNullException("Value is null");
            węzły.Remove(węzeł);
        }

        /// <summary>
        /// Funkcja zwracająca korzeń drzewa.
        /// </summary>
        /// <returns></returns>
        public IWęzeł<T> ZwróćKorzeń()
        {
            return korzeń;
        }

        /// <summary>
        /// Funkcja zwracająca wyznaczony węzeł
        /// </summary>
        /// <param name="węzeł"></param>
        /// <returns></returns>
        public IWęzeł<T> ZwróćWęzeł(T węzeł)
        {
            if (węzeł == null) throw new ArgumentNullException("Value is null");
            return węzły.FirstOrDefault(x => x.ZwróćWartość().Equals(węzeł));
        }

        /// <summary>
        /// Funkcja zwracająca wszystkie węzły drzewa
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IWęzeł<T>> ZwróćWęzły()
        {
            foreach (var węzeł in węzły)
            {
                yield return węzeł;
            }
        }
    }
}
