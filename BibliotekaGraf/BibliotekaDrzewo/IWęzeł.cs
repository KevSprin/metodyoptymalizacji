﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BibliotekaDrzewo
{
    public interface IWęzeł<T>
    {
        T ZwróćWartość();

        IEnumerable<IWęzeł<T>> ZwróćWęzły();

        void DodajWęzeł(ref IWęzeł<T> Węzeł);

        void DodajParent(ref IWęzeł<T> Węzeł);

        IWęzeł<T> ZwróćNastępny();

        IWęzeł<T> ZwróćParent();

        bool MaWęzły();

        bool Visited { get; set; }
    }
}
