﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BibliotekaDrzewo
{
    public interface IDrzewo<T>
    {
        void DodajWęzeł(IWęzeł<T> parent_węzeł, T węzeł_wartość);

        void DodajWęzeł(T parent_wartość, T węzeł_wartość);

        void UsuńWęzeł(IWęzeł<T> węzeł);

        IWęzeł<T> ZwróćWęzeł(T węzeł);

        IEnumerable<IWęzeł<T>> ZwróćWęzły();

        IWęzeł<T> this[int index]
        {
            get;
        }

        IWęzeł<T> ZwróćKorzeń();

        bool SprawdźParent(IWęzeł<T> parent, T nowy);
    }
}
