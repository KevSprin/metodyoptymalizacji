﻿using System;
using Komiwojażer;
using System.Collections.Generic;
using BibliotekaGraf;
using CyklEulera;
using MaksymalneSkojarzenie;
using AlgorytmWegierski;
using BibliotekaDrzewo;
using KolorowanieGrafu;
using System.Linq;

namespace ConsoleTestGraf
{
    public class Program
    {
        public static void WynokanAlgorytmWegierski()
        {
            GrafSkierowany<string, int> graf = new GrafSkierowany<string, int>();
            //graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6" });
            graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6", "7", "8" });
            graf.DodajKrawedz("1", "6", -3);
            graf.DodajKrawedz("1", "5", -8);
            graf.DodajKrawedz("1", "8", -9);
            graf.DodajKrawedz("2", "5", -4);
            graf.DodajKrawedz("2", "6", -7);
            graf.DodajKrawedz("2", "7", -14);
            graf.DodajKrawedz("3", "5", -8);
            graf.DodajKrawedz("3", "7", -4);
            graf.DodajKrawedz("3", "8", -10);
            graf.DodajKrawedz("4", "5", -3);
            graf.DodajKrawedz("4", "6", -7);
            //graf.DodajSkojarzenie("4", "3");
            //graf.DodajSkojarzenie("5", "2");
            graf.DodajDoZbioruV1(new[] { "1", "2", "3", "4" });
            graf.DodajDoZbioruV2(new[] { "8", "5", "6", "7" });
            AlgorytmWegierski<string> algorytmWegierski = new AlgorytmWegierski<string>(graf);
            int wynik = 0;
            var result_graf = algorytmWegierski.ZwrocGraf();
            foreach (var skojarzenie in algorytmWegierski.ZwrocGraf().ZwrocSkojarzenia())
            {

                Console.Write($"({skojarzenie.ZwrocPierwszy()}, {skojarzenie.ZwrocDrugi()}) ");                
            }
            foreach (var value in result_graf.ZwrocWagiSkojarzen())
            {
                wynik += value;
            }
            Console.WriteLine();
            Console.WriteLine("Wynik sumowania wag skojarzeń jest: " + wynik);
        }

        public static void WykonajMaksymalneSkojarzenie()
        {
            GrafNieskierowany<string, int> graf = new GrafNieskierowany<string, int>();
            graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6", "7", "8" });
            graf.DodajKrawedz("1", "5", 0);
            graf.DodajKrawedz("1", "6", 0);
            graf.DodajKrawedz("1", "7", 0);

            graf.DodajKrawedz("2", "5", 0);
            graf.DodajKrawedz("2", "6", 0);
            graf.DodajKrawedz("2", "8", 0);
            
            graf.DodajKrawedz("3", "5", 0);
            graf.DodajKrawedz("4", "6", 0);
            graf.DodajDoZbioruV1(new[] { "1", "2", "3", "4" });
            graf.DodajDoZbioruV2(new[] { "5", "6", "7", "8" });
            MaksymalneSkojarzenie<string, int> maks_skojarzenie = new MaksymalneSkojarzenie<string, int>(graf);
            var historia_skojarzen = maks_skojarzenie.ZnajdzMaksymalneSkojarzenie();
            foreach (var lista_skojarzen in historia_skojarzen)
            {
                Console.Write("{");
                foreach (var skojarzenie in lista_skojarzen)
                {
                    Console.Write("(" + skojarzenie.ZwrocPierwszy() + ", " + skojarzenie.ZwrocDrugi() + ") ");
                }
                Console.Write("}");
                Console.WriteLine();
            }
        }

        public static void CyklEuleraNieskierowany() {
            GrafNieskierowany<string, int> graf = new GrafNieskierowany<string, int>();
            graf.DodajWierzholek(new[] { "A", "B", "C", "D", "E" });
            graf.DodajKrawedz("A", "B", 0);
            graf.DodajKrawedz("A", "C", 0);
            graf.DodajKrawedz("B", "C", 0);
            graf.DodajKrawedz("C", "D", 0);
            graf.DodajKrawedz("C", "E", 0);
            graf.DodajKrawedz("D", "E", 0);
            CyklEulera<string, int> cyklEulera = new CyklEulera<string, int>(graf);
            var result = cyklEulera.Traverse();
            WyswietlWynik(result);
        }

        public static void CyklEuleraSkierowany()
        {
            GrafSkierowany<string, int> graf_s = new GrafSkierowany<string, int>();
            graf_s.DodajWierzholek(new[] { "A", "B", "C", "D", "E" });
            graf_s.DodajKrawedz("A", "C", 0);
            graf_s.DodajKrawedz("B", "A", 0);
            graf_s.DodajKrawedz("C", "B", 0);
            graf_s.DodajKrawedz("C", "E", 0);
            graf_s.DodajKrawedz("D", "C", 0);
            graf_s.DodajKrawedz("E", "D", 0);
            CyklEulera<string, int> cyklEulera = new CyklEulera<string, int>(graf_s);
            var result = cyklEulera.Traverse();
            WyswietlWynik(result);
        }

        public static void WyswietlWynik(Stack<string> s)
        {
            while (s.Count > 0)
            {
                Console.WriteLine(s.Peek().ToString());
                s.Pop();
            }
            Console.WriteLine();
        }

        public static bool traverse(IWęzeł<int> x)
        {
            if (x.Visited) return false;
            Console.WriteLine(x.ZwróćWartość());
            x.Visited = true;
            if (!x.MaWęzły()) return true;
            while (true)
            {
                var next = x.ZwróćNastępny();
                if (!traverse(next)) break;
            }
            return true;
        }

        private static void GetTreeValues(IWęzeł<int?> węzeł, ref List<int?> result)
        {
            if (węzeł == null) return;
            result.Add(węzeł.ZwróćWartość());
            GetTreeValues(węzeł.ZwróćParent(), ref result);
        }

        public static void WykonajKomiwojażera()
        {
            GrafNieskierowany<int, int> graf = new GrafNieskierowany<int, int>();
            graf.DodajWierzholek(new int[] { 0, 1, 2, 3, 4 });
            /*graf.DodajKrawedz(0, 1, 20);
            graf.DodajKrawedz(0, 2, 30);
            graf.DodajKrawedz(0, 3, 10);
            graf.DodajKrawedz(0, 4, 11);
            graf.DodajKrawedz(1, 2, 16);
            graf.DodajKrawedz(1, 3, 4);
            graf.DodajKrawedz(1, 4, 2);
            graf.DodajKrawedz(2, 3, 2);
            graf.DodajKrawedz(2, 4, 4);
            graf.DodajKrawedz(3, 4, 3);*/

            graf.DodajKrawedz(0, 1, 2);
            graf.DodajKrawedz(0, 2, 1);
            graf.DodajKrawedz(0, 3, 4);
            graf.DodajKrawedz(0, 4, 6);
            graf.DodajKrawedz(1, 2, 7);
            graf.DodajKrawedz(1, 3, 1);
            graf.DodajKrawedz(1, 4, 12);
            graf.DodajKrawedz(2, 3, 1);
            graf.DodajKrawedz(2, 4, 16);
            graf.DodajKrawedz(3, 4, 1);
            AlgorytmKomiwojażera tsp = new AlgorytmKomiwojażera(graf, true);
            var result = tsp.StartAlt();
            foreach (var edge in result.ZwróćWartość().GetEdges())
            {
                Console.WriteLine(edge.Start + " " + edge.Stop);
            }
            Console.WriteLine("Koszt minimalny wynosi: " + result.ZwróćWartość().Cost);
            /*
            List<int?> ścieżka = new List<int?>();
            GetTreeValues(result, ref ścieżka);
            for(int i = ścieżka.Count-1; i >= 0; --i)
            {
                Console.Write(ścieżka[i] + "->");
            }
            Console.Write(ścieżka[ścieżka.Count-1]);
            Console.WriteLine();
            Console.WriteLine($"Koszt całkowity ścieżki wynosi: {result.Cost.ZwróćWartość().Cost}");*/
        }

        public static void KolorujGraf()
        {
            GrafNieskierowany<string, int> graf = new GrafNieskierowany<string, int>();

            /*graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6" });
            graf.DodajKrawedz(new Para<string>("1", "4"));
            graf.DodajKrawedz(new Para<string>("4", "2"));
            graf.DodajKrawedz(new Para<string>("2", "5"));
            graf.DodajKrawedz(new Para<string>("2", "6"));
            graf.DodajKrawedz(new Para<string>("3", "4"));
            graf.DodajKrawedz(new Para<string>("3", "6"));*/

            /*graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6" });
            graf.DodajKrawedz(new Para<string>("1", "2"));
            graf.DodajKrawedz(new Para<string>("1", "4"));
            graf.DodajKrawedz(new Para<string>("1", "5"));
            graf.DodajKrawedz(new Para<string>("2", "3"));
            graf.DodajKrawedz(new Para<string>("2", "5"));
            graf.DodajKrawedz(new Para<string>("2", "6"));
            graf.DodajKrawedz(new Para<string>("3", "6"));
            graf.DodajKrawedz(new Para<string>("4", "5"));
            graf.DodajKrawedz(new Para<string>("5", "6"));*/

            /*graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6" });
            graf.DodajKrawedz(new Para<string>("1", "3"));
            graf.DodajKrawedz(new Para<string>("1", "4"));
            graf.DodajKrawedz(new Para<string>("1", "5"));
            graf.DodajKrawedz(new Para<string>("2", "4"));
            graf.DodajKrawedz(new Para<string>("2", "5"));
            graf.DodajKrawedz(new Para<string>("2", "6"));
            graf.DodajKrawedz(new Para<string>("3", "4"));
            graf.DodajKrawedz(new Para<string>("3", "6"));*/

            /*
            graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6", "7" });
            graf.DodajKrawedz(new Para<string>("1", "3"));
            graf.DodajKrawedz(new Para<string>("1", "4"));
            graf.DodajKrawedz(new Para<string>("1", "5"));
            graf.DodajKrawedz(new Para<string>("1", "7"));
            graf.DodajKrawedz(new Para<string>("2", "3"));
            graf.DodajKrawedz(new Para<string>("2", "4"));
            graf.DodajKrawedz(new Para<string>("2", "5"));
            graf.DodajKrawedz(new Para<string>("2", "6"));
            graf.DodajKrawedz(new Para<string>("3", "4"));
            graf.DodajKrawedz(new Para<string>("3", "7"));
            graf.DodajKrawedz(new Para<string>("4", "5"));
            graf.DodajKrawedz(new Para<string>("5", "6"));
            graf.DodajKrawedz(new Para<string>("6", "7"));
            */

            graf.DodajWierzholek(new[] { "1", "2", "3", "4", "5", "6" });
            graf.DodajKrawedz(new Para<string>("1", "2"));
            graf.DodajKrawedz(new Para<string>("1", "3"));
            graf.DodajKrawedz(new Para<string>("1", "4"));
            graf.DodajKrawedz(new Para<string>("1", "6"));
            graf.DodajKrawedz(new Para<string>("2", "5"));
            graf.DodajKrawedz(new Para<string>("2", "3"));
            graf.DodajKrawedz(new Para<string>("3", "4"));
            graf.DodajKrawedz(new Para<string>("4", "5"));
            
            KolorowanieGrafu<string> kolorowanieGrafu = new KolorowanieGrafu<string>(graf);
        }

        static void Main(string[] args)
        {
            //WykonajMaksymalneSkojarzenie();
            //WynokanAlgorytmWegierski();
            //WykonajKomiwojażera();
            KolorujGraf();
            Console.ReadKey();
            
        }
    }
}
