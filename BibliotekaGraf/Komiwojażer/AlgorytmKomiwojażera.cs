﻿using System;
using System.Collections.Generic;
using System.Linq;
using BibliotekaDrzewo;
using BibliotekaGraf;

namespace Komiwojażer
{

    public class Pair<T, K>
    {
        public T Value { get; set; }
        public K Cost { get; set; }
        public Pair(T Value, K Cost)
        {
            this.Value = Value;
            this.Cost = Cost;
        }
    }

    public class Edge<T>
    {
        public T Start { get; set; }
        public T Stop { get; set; }
        public Edge(T Start, T Stop)
        {
            this.Start = Start;
            this.Stop = Stop;
        }
    }

    public class Triplet<M, C, E>
    {
        public M Matrix { get; set; }
        public C Cost { get; set; }
        public List<E> Edges = new List<E>();

        public Triplet(M Matrix, C Cost)
        {
            this.Matrix = Matrix;
            this.Cost = Cost;
        }

        public Triplet(M Matrix, C Cost, E Edge)
        {
            this.Matrix = Matrix;
            this.Cost = Cost;
            if (Edge == null) return;
            Edges.Add(Edge);
        }

        public Triplet(M Matrix, C Cost, IEnumerable<E> Edges)
        {
            this.Matrix = Matrix;
            this.Cost = Cost;
            if (Edges == null) return;
            foreach (var edge in Edges)
            {
                if (edge == null) continue;
                this.Edges.Add(edge);
            }
        }

        public void AddEdge(E Edge)
        {
            if (Edge == null) return;
            Edges.Add(Edge);
        }

        public void AddEdges(IEnumerable<E> edges)
        {
            if (edges == null) return;
            foreach (var edge in edges)
            {
                if (edge == null) continue;
                Edges.Add(edge);
            }
        }

        public IEnumerable<E> GetEdges()
        {
            foreach (var edge in Edges)
            {
                yield return edge;
            }
        }
    }

    public class AlgorytmKomiwojażera
    {
        private GrafNieskierowany<int, int> graf;
        private IDrzewo<int?> drzewo;
        private IDrzewo<Pair<int?[,], int?>> bound_drzewo;
        private IDrzewo<Triplet<int?[,], int?, Edge<int?>>> Drzewo_podziału;

        public AlgorytmKomiwojażera(GrafNieskierowany<int, int> graf)
        {
            this.graf = graf;
            int n = graf.IloscWierzholkow();
            drzewo = new Drzewo<int?>(graf.ZwrocPierwszyWierzcholek());

            var macierz = InicjalizujMacierz(n);
            macierz = GrafToMacierz(macierz);
            var macierz_zredukowana = Redukcja(macierz);
            bound_drzewo = new Drzewo<Pair<int?[,], int?>>(macierz_zredukowana);
        }

        public AlgorytmKomiwojażera(GrafNieskierowany<int, int> graf, bool tmp)
        {
            this.graf = graf;
            int n = graf.IloscWierzholkow();

            var macierz = InicjalizujMacierz(n);
            macierz = GrafToMacierz(macierz);
            var macierz_zredukowana = Redukcja(macierz);
            var korzeń = new Triplet<int?[,], int?, Edge<int?>>(macierz_zredukowana.Value, macierz_zredukowana.Cost);
            Drzewo_podziału = new Drzewo<Triplet<int?[,], int?, Edge<int?>>>(korzeń);
        }

        public IWęzeł<Triplet<int?[,], int?, Edge<int?>>> StartAlt()
        {
            IWęzeł<int?> result_values = null;
            IWęzeł<Triplet<int?[,], int?, Edge<int?>>> result = null;
            while (true)
            {
                int? index_i, index_j;

                // Szukamy liść o najmniejszym koszcie naszego drzewa
                var węzeł_najmniejszego_kosztu = NajtańszyLiśćDrzewaPodziału();

                // Sprawdzamy czy nasza macierz nie jest już końcowym wynikiem
                if (SprawdźMacierz(węzeł_najmniejszego_kosztu.ZwróćWartość().Matrix))
                {
                    result = węzeł_najmniejszego_kosztu;
                    break;
                }
                // Szukamy takiego zera w macierzy, gdzie koszt z kolumny i wiersza danego zera jest najmniejszy
                ZwróćIndeksyNajmniejszegoZera(węzeł_najmniejszego_kosztu.ZwróćWartość().Matrix, out index_i, out index_j);

                // Wypełniamy odpowiednie wiersze i kolumny nieskończonością (w naszym przypadku nullami
                var macierz_zredukowana = RedukcjaWymiaru(węzeł_najmniejszego_kosztu.ZwróćWartość().Matrix, index_i.Value, index_j.Value);

                // Dokonujemy redukcji
                var wynik_redukcji = Redukcja(macierz_zredukowana);

                // Pobieramy koszt przejścia po krawędzi między jednym wierzchołkiem a drugim (koszt z poprzedniej macierzy)
                var koszt_między_wierzchołkami = węzeł_najmniejszego_kosztu.ZwróćWartość().Matrix[index_i.Value, index_j.Value];

                // Sumujemy koszta
                var koszt_całkowity = koszt_między_wierzchołkami + wynik_redukcji.Cost + węzeł_najmniejszego_kosztu.ZwróćWartość().Cost;

                // pobieramy wcześniejsze połączenia
                var krawędzie = węzeł_najmniejszego_kosztu.ZwróćWartość().GetEdges();

                // tworzymy nowy wpis do drzewa z boundami               
                var nowy_wpis = new Triplet<int?[,], int?, Edge<int?>>(wynik_redukcji.Value, koszt_całkowity, krawędzie);

                // tworzymy i dodajemy krawędź
                Edge<int?> edge = new Edge<int?>(index_i, index_j);
                nowy_wpis.AddEdge(edge);

                // Dodajemy wpis wybranego rozwiązania do drzewa
                Drzewo_podziału.DodajWęzeł(węzeł_najmniejszego_kosztu, nowy_wpis);

                // dla pozostałch rozwiązań
                var macierz = (int?[,])węzeł_najmniejszego_kosztu.ZwróćWartość().Matrix.Clone();

                // wpisujemy nieskonczoność w przypadku gdzie chcemy wykluczyć, wybrane w pierwszej gałęzi, rozwiązanie
                macierz[index_i.Value, index_j.Value] = null;

                // zwracamy koszt i macierz dla pozostałych wyników
                var macierz_koszt_pozostałych = ZwróćMacierzPozostałych(macierz, index_i.Value, index_j.Value);

                // dodajemy do kosztu macierzy pozostałych koszt poprzedniego wpisu
                macierz_koszt_pozostałych.Cost += węzeł_najmniejszego_kosztu.ZwróćWartość().Cost.Value;

                // nowy wpis do drzewa na podstawie macierzy pozostałych, kosztu oraz krawędzi poprzedniego wpisu
                var nowy_wpis_pozostałych = new Triplet<int?[,], int?, Edge<int?>>(macierz_koszt_pozostałych.Value, macierz_koszt_pozostałych.Cost, krawędzie);

                // dodawanie macierzy pozostałych do drzewa
                Drzewo_podziału.DodajWęzeł(węzeł_najmniejszego_kosztu, nowy_wpis_pozostałych);

            }
            return result;
        }

        /// <summary>
        /// Funkcja zwracającą zredukowaną macierz pozostałych oraz jej koszt
        /// </summary>
        /// <param name="macierz"></param>
        /// <param name="index_i"></param>
        /// <param name="index_j"></param>
        /// <returns></returns>
        private Pair<int?[,], int?> ZwróćMacierzPozostałych(int?[,] macierz, int index_i, int index_j)
        {
            int? row_min = GetMinRow(macierz, index_i);
            int? col_min = GetMinCol(macierz, index_j);
            for (int i = index_i; i < macierz.GetLength(0); ++i)
            {
                macierz[i, index_j] -= col_min;
            }
            for (int j = index_j; j < macierz.GetLength(0); ++j)
            {
                macierz[index_i, j] -= row_min;
            }
            int? koszt = row_min + col_min;
            var result = new Pair<int?[,], int?>(macierz, koszt);
            return result;
        }

        private bool SprawdźMacierz(int?[,] macierz)
        {
            for(int i = 0; i < macierz.GetLength(0); ++i)
            {
                for(int j = 0; j < macierz.GetLength(1); ++j)
                {
                    if (macierz[i, j] != null) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Funkcja zwracająca element bound_drzewa o najmniejszym koszcie.
        /// </summary>
        /// <returns></returns>
        private IWęzeł<Triplet<int?[,], int?, Edge<int?>>> NajtańszyLiśćDrzewaPodziału()
        {
            IWęzeł<Triplet<int?[,], int?, Edge<int?>>> result = null;
            foreach (var węzeł in Drzewo_podziału.ZwróćWęzły().Where(x => x.Visited == false))
            {
                if (result == null) result = węzeł;
                if (result.ZwróćWartość().Cost > węzeł.ZwróćWartość().Cost)
                    result = węzeł;
            }
            result.Visited = true;
            return result;
        }

        /// <summary>
        /// Funkcja zwracająca indeksy komórki macierzy zredukowanej, w której znajduje się najtańsze zero
        /// </summary>
        /// <param name="macierz"></param>
        /// <param name="index_i"></param>
        /// <param name="index_j"></param>
        public void ZwróćIndeksyNajmniejszegoZera(int?[,] macierz, out int? index_i, out int? index_j)
        {
            index_i = null;
            index_j = null;
            int tmp_cost = int.MaxValue;
            for (int i = 0; i < macierz.GetLength(0); ++i)
            {            
                for (int j = 0; j < macierz.GetLength(1); ++j)
                {
                    if (macierz[i,j] == 0)
                    {
                        int koszt_ij = ZwróćKosztMacierzyWDanymWierszu(macierz, i, j);
                        if (tmp_cost > koszt_ij)
                        {
                            tmp_cost = koszt_ij;
                            index_i = i;
                            index_j = j;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja zwracająca koszt "zera" w macierzy zredukowanej.
        /// </summary>
        /// <param name="macierz"></param>
        /// <param name="index_i"></param>
        /// <param name="index_j"></param>
        /// <returns></returns>
        public int ZwróćKosztMacierzyWDanymWierszu(int?[,] macierz, int index_i, int index_j)
        {
            int result_col = 0, result_row = 0;
            for(int i = 0; i < macierz.GetLength(0); ++i)
            {
                if (macierz[i, index_j] == null) continue;
                result_col += macierz[i, index_j].Value;
            }
            for(int j = 0; j < macierz.GetLength(0); ++j)
            {
                if (macierz[index_i, j] == null) continue;
                result_row += macierz[index_i, j].Value;
            }
            return result_col + result_row;
        }

        /// <summary>
        /// Funkcja startująca obliczanie przybliżonego optymalnego rozwiązania
        /// </summary>
        /// <returns></returns>
        public Pair<IWęzeł<int?>, IWęzeł<Pair<int?[,], int?>>> Start()
        {
            IWęzeł<int?> result_values = null;
            IWęzeł<Pair<int?[,], int?>> result_bounds = null;
            while (true)
            {
                int index = bound_drzewo.ZwróćWęzły().ToList().IndexOf(SmallestCost());
                var węzeł_najmniejszego_kosztu = bound_drzewo[index];
                var wierzchołek_start = drzewo[index];
                ExpandTree(wierzchołek_start);
                if (wierzchołek_start.ZwróćWęzły().Count() == 0)
                {
                    result_values = wierzchołek_start;
                    result_bounds = węzeł_najmniejszego_kosztu;
                    break;
                }
                foreach (var wierzchołek_stop in wierzchołek_start.ZwróćWęzły())
                {
                    // Wypełniamy odpowiednie wiersze i kolumny nieskończonością (w naszym przypadku nullami
                    var macierz_zredukowana = RedukcjaWymiaru(węzeł_najmniejszego_kosztu.ZwróćWartość().Value, wierzchołek_start.ZwróćWartość().Value, wierzchołek_stop.ZwróćWartość().Value);

                    // Dokonujemy redukcji
                    var wynik_redukcji = Redukcja(macierz_zredukowana);

                    // Pobieramy koszt przejścia po krawędzi między jednym wierzchołkiem a drugim (koszt z poprzedniej macierzy)
                    var koszt_między_wierzchołkami = węzeł_najmniejszego_kosztu.ZwróćWartość().Value[wierzchołek_start.ZwróćWartość().Value, wierzchołek_stop.ZwróćWartość().Value];

                    // Sumujemy koszta
                    var koszt_całkowity = koszt_między_wierzchołkami + wynik_redukcji.Cost + węzeł_najmniejszego_kosztu.ZwróćWartość().Cost;

                    // tworzymy nowy wpis do drzewa z boundami
                    var nowy_wpis = new Węzeł<Pair<int?[,], int?>>(new Pair<int?[,], int?>(wynik_redukcji.Value, koszt_całkowity));
                    bound_drzewo.DodajWęzeł(węzeł_najmniejszego_kosztu, nowy_wpis.ZwróćWartość());
                }
            }
            return new Pair<IWęzeł<int?>, IWęzeł<Pair<int?[,], int?>>>(result_values, result_bounds);
        }

        /// <summary>
        /// Funkcja zwracająca macierz, w której wiersze i kolumny zostały zastąpione nieskończonościa (a tak naprawdę nullami). Wiersze i kolumny odpowiadają wierzchołkom.
        /// </summary>
        /// <param name="macierz"></param>
        /// <param name="wierzchołek_start"></param>
        /// <param name="wierzchołek_stop"></param>
        /// <returns></returns>
        private int?[,] RedukcjaWymiaru(int?[,] macierz, int wierzchołek_start, int wierzchołek_stop)
        {
            int?[,] nowa_macierz = (int?[,])macierz.Clone();
            int w_start = wierzchołek_start;
            int w_stop = wierzchołek_stop;
            for (int i = 0; i < nowa_macierz.GetLength(0); i++)
            {
                nowa_macierz[w_start, i] = null;
                nowa_macierz[i, w_stop] = null;
            }
            nowa_macierz[w_stop, w_start] = null;
            return nowa_macierz;
        }

        /// <summary>
        /// Funkcja zwracająca element bound_drzewa o najmniejszym koszcie.
        /// </summary>
        /// <returns></returns>
        private IWęzeł<Pair<int?[,], int?>> SmallestCost()
        {
            IWęzeł<Pair<int?[,], int?>> result = null;          
            foreach (var węzeł in bound_drzewo.ZwróćWęzły().Where(x => x.Visited == false))
            {
                if (result == null) result = węzeł;
                if (result.ZwróćWartość().Cost > węzeł.ZwróćWartość().Cost)
                    result = węzeł;
            }
            return result;
        }

        /// <summary>
        /// Funkcja odpowiadająca za tworzenie nowych gałęzi naszego drzewa.
        /// </summary>
        /// <param name="wierzcholek"></param>
        private void ExpandTree(IWęzeł<int?> wierzcholek)
        {
            if (wierzcholek == null) return;
            foreach (var sąsiad in graf.ZwrocSasiadow(wierzcholek.ZwróćWartość().Value))
            {
                if (!drzewo.SprawdźParent(wierzcholek, sąsiad))
                    drzewo.DodajWęzeł(wierzcholek, sąsiad);
            }

            int index = drzewo.ZwróćWęzły().ToList().IndexOf(wierzcholek);
            drzewo[index].Visited = true;
            bound_drzewo[index].Visited = true;
        }

        /// <summary>
        /// Funkcja inicjalizująca macierz początkową.
        /// </summary>
        private int?[,] InicjalizujMacierz(int n)
        {
            int? [,] macierz = new int?[n, n];
            for (int i = 0; i < macierz.GetLength(0); i++)
            {
                for (int j = 0; j < macierz.GetLength(1); j++)
                {
                    macierz[i, j] = null;
                }
            }
            return macierz;
        }

        /// <summary>
        /// Funkcja odpowiadająca za przekonwertowanie grafu do postaci macierzy.
        /// </summary>
        private int?[,] GrafToMacierz(int?[,] macierz)
        {
            foreach (var wierzcholek in graf.ZwrocWierzholki())
            {
                foreach (var sąsiad in graf.ZwrocSasiadow(wierzcholek))
                {
                    macierz[wierzcholek, sąsiad] = graf.ZwrocWage(wierzcholek, sąsiad);
                }
            }
            return macierz;
        }

        /// <summary>
        /// Funkcja obliczająca macierzy zredukowanej.
        /// </summary>
        /// <param name="macierz"></param>
        /// <returns></returns>
        private Pair<int?[,], int?> Redukcja(int?[,] macierz)
        {
            List<int?> row_minimum = new List<int?>();
            List<int?> col_minimum = new List<int?>();
            int?[,] nowa_macierz = (int?[,])macierz.Clone();
            for (int i = 0; i < nowa_macierz.GetLength(0); i++)
            {
                int? minimum = GetMinRow(nowa_macierz, i);
                if (minimum == null) continue;
                row_minimum.Add(minimum);
                for (int j = 0; j < nowa_macierz.GetLength(1); j++)
                {
                    if (i == j || nowa_macierz[i, j] == null) continue;
                    nowa_macierz[i, j] -= minimum;
                }
            }
            for (int i = 0; i < nowa_macierz.GetLength(0); i++)
            {
                int? minimum = GetMinCol(nowa_macierz, i);
                if (minimum == null) continue;
                col_minimum.Add(minimum);
                for (int j = 0; j < nowa_macierz.GetLength(1); j++)
                {
                    if (i == j || nowa_macierz[j, i] == null) continue;
                    nowa_macierz[j, i] -= minimum;
                }
            }
            int? cost_row = row_minimum.Sum();
            int? cost_col = col_minimum.Sum();
            int? cost = cost_col + cost_row;

            Pair<int?[,], int?> result = new Pair<int?[,], int?>(nowa_macierz, cost);
            return result;
        }

        /// <summary>
        /// Funkcja zwracająca najmniejszą wartość wiersza.
        /// </summary>
        /// <param name="macierz"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private int? GetMinRow(int?[,] macierz, int row)
        {
            int? result = null;
            for (int i = 0; i < macierz.GetLength(1); i++)
            {
                if (result > macierz[row, i] || result == null)
                    result = macierz[row, i];
            }
            return result;
        }

        /// <summary>
        /// Funkcja zwracająca najmniejszą wartość kolumny.
        /// </summary>
        /// <param name="macierz"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private int? GetMinCol(int?[,] macierz, int col)
        {
            int? result = null;
            for (int i = 0; i < macierz.GetLength(1); i++)
            {
                if (result > macierz[i, col] || result == null)
                    result = macierz[i, col];
            }
            return result;
        }
    }
}