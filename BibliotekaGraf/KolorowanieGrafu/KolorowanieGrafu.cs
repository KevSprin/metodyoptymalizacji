﻿using System;
using System.Collections.Generic;
using System.Linq;
using BibliotekaDrzewo;
using BibliotekaGraf;


namespace KolorowanieGrafu
{
    public class KolorowanieGrafu<T>
    {
        private GrafNieskierowany<T, int> graf;
        private Drzewo<Triplet> drzewo_kolorow;

        public class Triplet : IComparable
        {
            public T Key { private set; get; }
            public int Kolor { private set; get; }
            public int Depth { private set; get; }

            public Triplet()
            {

            }
            public Triplet(T Key, int Kolor, int Depth)
            {
                this.Key = Key;
                this.Kolor = Kolor;
                this.Depth = Depth;
            }

            public int CompareTo(object obj)
            {
                Triplet other = obj as Triplet;
                if (Kolor < other.Kolor) return -1;
                else if (Kolor == other.Kolor) return 0;
                else return 1;
            }
        }

        public KolorowanieGrafu(GrafNieskierowany<T, int> graf)
        {
            this.graf = graf;
            graf.UstawWierzcholkiLF();
            var rozwiazanie = Koloruj();
            TraverseParents(rozwiazanie);
        }
        
        private void TraverseParents(IWęzeł<Triplet> węzeł)
        {
            if (węzeł == null) return; 
            Console.WriteLine("Wierzchołek: " + węzeł.ZwróćWartość().Key + " -> Kolor: "+ węzeł.ZwróćWartość().Kolor);
            TraverseParents(węzeł.ZwróćParent());
        }

        private int ZnajdzNajmniejszeI(int l)
        {
            foreach (var wezel in drzewo_kolorow.ZwróćWęzły())
            {
                if (wezel.ZwróćWartość().Kolor == l) return wezel.ZwróćWartość().Depth;
            }
            return -1;
        }

        private void UsunKolory(int start, int stop)
        {
            
            foreach (var wierzcholek in graf.ZwrocWierzholki())
            {
                if (wierzcholek.Equals(graf.ZwrocPierwszyWierzcholek())) continue;
                for (int i = start; i < stop; ++i)
                {
                    graf.UsunKolor(wierzcholek, i);
                }
            }
            
            /*
            foreach (var wierzcholek in graf.ZwrocWierzholki())
            {
                if (wierzcholek.Equals(graf.ZwrocPierwszyWierzcholek())) continue;
                graf.UsunKolor(wierzcholek, start);
                start++;
            }*/
        }

        /// <summary>
        /// Funkcja zwracająca zbiór dopuszczalnych kolorów dla rozpatrywanego wierzchołka.
        /// </summary>
        /// <param name="wierzcholek"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        private List<int> ZwrocDopuszczalneKoloryWierzcholka(T wierzcholek, int k, int l)
        {
            if (wierzcholek == null) throw new ArgumentNullException("Wierzcholek jest pusty");
            List<int> dostepne_kolory = new List<int>();

            // zwraca już istniejące koloru rozpatrywanego wierzchołka o ile istnieją
            if (graf.ZwrocZbiorKolorówDopuszczalnych(wierzcholek) != null)
                dostepne_kolory = graf.ZwrocZbiorKolorówDopuszczalnych(wierzcholek).ToList();

            var wierzcholki = graf.ZwrocWierzholki().ToList();

            // pobieramy poprzedni wierzchołek aby sprawdzić maksymalny kolor
            T wierzcholek_poprzedni = graf[wierzcholki.IndexOf(wierzcholek) - 1];


            dostepne_kolory.Add(l + 1);
            // Sprawdzamy wszystkie istniejące kolory z grafu
            foreach (var w in wierzcholki)
            {
                var kolor_wierzcholka_w = graf.ZwrocKolor(w);
                if (!dostepne_kolory.Contains(kolor_wierzcholka_w) && kolor_wierzcholka_w != 0)
                {
                    // j <= l_i-1 + 1
                    if (kolor_wierzcholka_w > graf.ZwrocMaksymalnyKolorWierzcholka(wierzcholek_poprzedni) + 1) continue; 

                    List<int> tmp = new List<int> { k, graf.ZwrocSasiadow(wierzcholek).Count() + 1 };
                    // j <= min{ k, | S_k | + 1}
                    if (kolor_wierzcholka_w > tmp.Min()) continue; 

                    tmp = graf.ZwrocKolorySasiadowWierzcholka(wierzcholek).ToList();
                    // j nie jest kolorem żadnego wierzchołka sasiedniego
                    if (tmp.Contains(kolor_wierzcholka_w)) continue;

                    // dodajemy kolor do zbioru jeśli spełnił warunki
                    dostepne_kolory.Add(kolor_wierzcholka_w); 
                }
            }

            
            graf.DodajDopuszczalneKolory(wierzcholek, dostepne_kolory);
            return dostepne_kolory;
        }

        public IWęzeł<Triplet> Koloruj()
        {
            SortedList<Triplet, int> liscie = new SortedList<Triplet, int>();
            int l = 1, k = 1, n = graf.ZwrocWierzholki().Count(), q = n + 1, lowerbound = int.MaxValue;
            bool increase = true;
            graf.DodajKolor(graf[0], 1);         
            List<int> tmp_dopuszczalne_kolory = new List<int>();
            Triplet tmp_wpis = new Triplet(graf[0], 1, k);
            drzewo_kolorow = new Drzewo<Triplet>(tmp_wpis);
            do
            {
                if (increase)
                {
                    graf.DodajMaksymalnyKolor(graf[k - 1], l);
                    k++;
                    tmp_dopuszczalne_kolory = ZwrocDopuszczalneKoloryWierzcholka(graf[k - 1], k, l);
                }

                if(tmp_dopuszczalne_kolory.Count == 0)
                {
                    k--;
                    l = graf.ZwrocMaksymalnyKolorWierzcholka(graf[k - 1]);
                    increase = false;
                }
                else
                {
                    int j = tmp_dopuszczalne_kolory.Min();
                    tmp_dopuszczalne_kolory.Remove(j);
                    graf.UsunKolor(graf[k -1], j);

                    graf.DodajKolor(graf[k - 1], j);
                    Triplet wierzcholek_kolor = new Triplet(graf[k - 1], j, k);
                    drzewo_kolorow.DodajWęzeł(tmp_wpis, wierzcholek_kolor);
                    tmp_wpis = wierzcholek_kolor;
                    if (j > l) l++;

                    if (k < n) increase = true;
                    else
                    {                  
                        int i = ZnajdzNajmniejszeI(l);
                        if (i == -1) throw new ArgumentNullException("Nie znalazł wierzcholka o kolorze równym do l");
                        UsunKolory(l, q - 1);
                        q = l;
                        if(lowerbound > q)
                            lowerbound = q;
                        l = q - 1;
                        k = i - 1;
                        // Zapamiętanie rozwiązania
                        liscie.Add(wierzcholek_kolor, q);
                        tmp_dopuszczalne_kolory = graf.ZwrocZbiorKolorówDopuszczalnych(graf[k - 1]).ToList();
                        tmp_wpis = drzewo_kolorow.ZwróćWęzeł(wierzcholek_kolor).ZwróćParent().ZwróćParent().ZwróćWartość();
                        increase = false;
                    }
                }

            } while (k != 1 && l != lowerbound);
            var najlepsze_rozwiazanie = liscie.OrderBy(x => x.Value).Select(x => x.Key).ToList().First();
            var najlepszy_wynik = liscie.OrderBy(x => x.Value).ToList().First().Value;
            Console.WriteLine("q = " + najlepszy_wynik);
            return drzewo_kolorow.ZwróćWęzeł(najlepsze_rozwiazanie);
        }
    }
}
