﻿using System;
using System.Collections.Generic;
using System.Linq;
using BibliotekaGraf;

namespace MaksymalneSkojarzenie
{
    public class MaksymalneSkojarzenie<T, K>
    {
        private GrafAbstrakcyjny<T, K> graf_skierowany;
        private List<T> zbiorwolnych_v1 = new List<T>();
        private List<T> zbiorwolnych_v2 = new List<T>();

        /// <summary>
        /// Konstruktor klasy Maksymalnego skojarzenia.
        /// </summary>
        /// <param name="graf_nieskierowany"></param>
        public MaksymalneSkojarzenie(GrafNieskierowany<T, K> graf_nieskierowany) // Tworzymy graf skierowany na podstawie grafu nieskierowanego
        {
            if (graf_nieskierowany.ZwrocKrawedzie() == null || graf_nieskierowany.ZwrocWierzholki() == null)
                throw new ArgumentNullException("Graf nie nadaje się do Maksymalnego skojarzenia!");

            graf_skierowany = new GrafSkierowany<T, K>();
            graf_skierowany.KopiujSkojarzeniaZbiory(graf_nieskierowany); // kopiuje skojarzenia oraz podzbiory V1 i V2
            graf_skierowany.DodajWierzholek(graf_nieskierowany.ZwrocWierzholki()); // kopiuje wierzchołki do grafu skierowanego

            foreach (var krawedz in graf_nieskierowany.ZwrocNieskojarzoneKrawedzie())
            {
                graf_skierowany.DodajKrawedz(krawedz); // tworzymy krawedzie z nieskojarzonych krawedzi
            }
            foreach (var skojarzenie in graf_nieskierowany.ZwrocSkojarzenia())
            {
                graf_skierowany.DodajKrawedz(skojarzenie); // tworzymy krawedzie z skojarzonych krawedzi 
            }
        }

        /// <summary>
        /// Alternatywny konstruktor do przekazania gotowego grafu.
        /// </summary>
        /// <param name="graf_skierowany"></param>
        public MaksymalneSkojarzenie(GrafAbstrakcyjny<T, K> graf_skierowany)
        {
            this.graf_skierowany = graf_skierowany;
        }

        public GrafAbstrakcyjny<T, K> ZwrocGraf()
        {
            return graf_skierowany;
        }

        /// <summary>
        /// Główna funkcja algorytmu znajdywania maksymalnego skojarzenia 
        /// </summary>
        public IList<List<IPara<T>>> ZnajdzMaksymalneSkojarzenie()
        {
            List<List<IPara<T>>> historia_skojarzeń = new List<List<IPara<T>>>();
            IEnumerable<IPara<T>> sciezka_powiekszajaca = new List<IPara<T>>();
            do
            {
                sciezka_powiekszajaca = ZnajdzSciezkePowiekszajaca();
                historia_skojarzeń.Add(graf_skierowany.ZwrocSkojarzenia().ToList());
                if (sciezka_powiekszajaca != null)
                {
                    // suma zbiorów odjąc iloczyn zbiorów 
                    graf_skierowany.SumaProsta(sciezka_powiekszajaca.ToList());                   
                }
            } while (sciezka_powiekszajaca != null);
            return historia_skojarzeń;
        }

        /// <summary>
        /// Główna funkcja algorytmu znajdywania skojarzenia początkowego (w zależności od przypadku może nawet pełne skojarzenie znaleźć)
        /// </summary>
        public void ZnajdzSkojarzeniePoczatkowe()
        {
            if (graf_skierowany.ZwrocSkojarzenia().Count() > 0) throw new ArgumentException("Graf już posiada skojarzenia. Nalezy przygotowac grad bez skojarzen");
            zbiorwolnych_v1 = graf_skierowany.ZwrocZbiorWolnychV1().ToList();  // --- przygotowujemy zbiór wolnych wierzchołków zbioru V1          
            foreach (var wierzcholek in zbiorwolnych_v1) 
            {
                zbiorwolnych_v2 = graf_skierowany.ZwrocZbiorWolnychV2().ToList();  // --- aktualizujemy zbiór wolnych wierzchołków V2
                var sasiedzi = graf_skierowany.ZwrocSasiadow(wierzcholek); // --- zbieramy sąsiadów danego wierzchołka
                foreach(var sasiad in sasiedzi)
                {
                    if (zbiorwolnych_v2.Contains(sasiad))   // --- jeśli sąsiad wolnego wierzchołka z v1 jest zawarty w zbiorze wolnych v2 to tworzymy między nimi skojarzenie 
                    {
                        graf_skierowany.DodajSkojarzenie(sasiad, wierzcholek);
                        break;                              
                        // -- wychodzimy z pętli sąsiadów, ponieważ mamy już skojarzenie dla danego wierzchołka z v1
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja do znajdywania ścieżki powiększającej
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IPara<T>> ZnajdzSciezkePowiekszajaca()
        {
            // --- szukanie ścieżki powiększającej
            // --- tworzymy zbiór wierzchołków wolnych
            zbiorwolnych_v1 = graf_skierowany.ZwrocZbiorWolnychV1().ToList();
            zbiorwolnych_v2 = graf_skierowany.ZwrocZbiorWolnychV2().ToList();
            List<Stack<T>> zbior_sciezek = new List<Stack<T>>();
            foreach (var wierzcholek in zbiorwolnych_v1)
            {
                //result = new Queue<T>();
                Stack<T> result = new Stack<T>();
                DFS(wierzcholek, result);
                if (result.Count < 2) continue;
                zbior_sciezek.Add(result);
            }
            // --- NIL jeśli nie ma już ścieżki powiększającej
            if (zbior_sciezek.Count == 0) return null;

            // --- wyłuskanie największej ścieżki powiększającej (według największej ilości wierzchołków)
            Stack<T> najwieksza_sciezka = new Stack<T>();
            foreach (var sciezka in zbior_sciezek)
            {
                if (najwieksza_sciezka.Count < sciezka.Count || najwieksza_sciezka == null) najwieksza_sciezka = sciezka;
            }

            // --- tworzenie par z scieżki do skojarzeń
            List<IPara<T>> sciezka_powiekszajaca = new List<IPara<T>>();
            T cel = najwieksza_sciezka.Pop();
            while (najwieksza_sciezka.Count > 0)
            {
                var start = najwieksza_sciezka.Pop();
                IPara<T> para = new Para<T>(start, cel);
                sciezka_powiekszajaca.Insert(0, para);
                cel = start;
            }
            return sciezka_powiekszajaca;
        }

        private Stack<T> Expand(T wierzcholek, ref Stack<T> sciezka)
        {
            if (wierzcholek == null) return null;
            Stack<T> return_stack = new Stack<T>();
            // zwracamy sąsidów danego wierzchołka
            foreach (var sasiad in graf_skierowany.ZwrocSasiadow(wierzcholek))
            {
                // zabezpieczenie przed cyklami
                if (!sciezka.Contains(sasiad)) return_stack.Push(sasiad);
            }
            // dodajemy dotyczasowy wierzcholek do stosu ścieżki
            sciezka.Push(wierzcholek); 
            return return_stack;
        }

        private bool DFS(T wierzcholek, Stack<T> sciezka) // szukanie ścieżki za pomocą Algorytmu Depth-First-Search
        {
            if (zbiorwolnych_v2.Contains(wierzcholek))
            {
                sciezka.Push(wierzcholek);
                return true;
            }
            Stack<T> expand_stack = Expand(wierzcholek,ref sciezka);
            while (expand_stack.Count > 0)
            {
                var popped_element = expand_stack.Pop();
                if (DFS(popped_element, sciezka)) return true;
                if (sciezka.Peek().Equals(popped_element)) sciezka.Pop(); 
            }
            return false;
        }
    }
}
