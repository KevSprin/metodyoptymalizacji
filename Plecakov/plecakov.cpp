#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <algorithm>

using namespace std;

class Lisc{
    private:
        int wartosc;
        int current_sasiad = 0;
        vector<Lisc*> sasiedzi;
    public:
        Lisc();
        Lisc(int n){
            wartosc = n;
        }

        Lisc* DodajSasiada(int n){
            Lisc *sasiad = new Lisc(n);
            sasiedzi.push_back(sasiad);
            return sasiad;
        }

        void DodajSasiada(Lisc *sasiad){
            sasiedzi.push_back(sasiad);
        }

        Lisc* GetSasiad(int n){
            if(sasiedzi.empty()) return NULL;
            return sasiedzi[n];
        }

        int GetWartosc(){
            return wartosc;
        }

        int GetSasiedziNum(){
            return sasiedzi.size();
        }

        ~Lisc(){
        }
};

class DrzewoPermutacji{
    private:
        Lisc *korzen;
        vector<Lisc*> liscie;
    public:
        DrzewoPermutacji(){
            korzen = new Lisc(0);
            liscie.push_back(korzen);
        }
        DrzewoPermutacji(int n){
            korzen = new Lisc(0);
            liscie.push_back(korzen);
            for(int i = 1; i < n; ++i){
                liscie.push_back(korzen->DodajSasiada(i));
            }
            for(int i = 1; i < liscie.size(); ++i){
                for(int j = i+1; j < liscie.size(); ++j){
                    liscie[i]->DodajSasiada(liscie[j]);
                }
            }
        }
        Lisc* ZnajdzWartosc(int n){
            for(int i = 0 ; i < liscie.size(); ++i){
                if(liscie[i]->GetWartosc() == n) return liscie[i];
            }
        }
        // tworzenie drzewa / albo raczej grafu skierowanego permutacji
        void Permutacja(int k){ 
            Permutacje = vector<vector<int>>();
            for(int i = 0; i < liscie.size(); ++i){
                vector<int> tmp;
                Traverse(liscie[i], 0, k, tmp);
            }
        }

        vector<vector<int>> Permutacje;
        // Przeszukiwanie permutacji
        bool Traverse(Lisc* lisc, int depth, int k, vector<int> sciezka){
            if(depth == k){
                sciezka.push_back(lisc->GetWartosc());
                Permutacje.push_back(sciezka);
                return true;
            }
            if(lisc == NULL && depth != k) return false;
            sciezka.push_back(lisc->GetWartosc());
            for(int i = 0; i < lisc->GetSasiedziNum(); ++i){
                Traverse(lisc->GetSasiad(i), depth+1, k, sciezka);
            }   
            return true;            
        }

        void MniejszyRownyUdzwig(int udzwig, vector<int> wagi){
            vector<int> indexy;
            for(int i = 0; i < Permutacje.size(); ++i){
                int suma = 0;
                for(int j = 0; j < Permutacje[0].size(); ++j){
                    suma += wagi[Permutacje[i].at(j)];
                }
                if(suma <= udzwig){
                    indexy.push_back(i);
                }                   
            }
            // wybieranie wyłącznie permutacji o mniejszym koszcie
            vector<vector<int>> tmp;
            for(int i = 0; i < indexy.size(); ++i){
                tmp.push_back(Permutacje[indexy[i]]);
            }
            Permutacje = tmp;
        }

        void PrintPermutacje(){
            for(int i = 0; i < Permutacje.size(); ++i){
                for(int j = 0; j < Permutacje[0].size(); ++j){
                    cout << Permutacje[i].at(j) << " ";
                }
                cout << endl;
            }
        }

        ~DrzewoPermutacji(){
            delete korzen;
            for(int i = 1; i < liscie.size(); ++i){
                delete liscie[i];
            }
        }
};

void WyswietlWielomian(vector<int> wartosc_przedmiotow, vector<int> wagi_przedmiotow, int udzwig){
    int n = wartosc_przedmiotow.size();
    system("cls");
    cout << "max z = ";
    for(int i = 0; i < n; ++i){
        if(i > 0)
            cout << " + ";
        cout << wartosc_przedmiotow[i] << "x" << (i+1);
    }
    cout << endl;
    for(int i = 0; i < n; ++i){
        if(i > 0)
            cout << " + ";
        cout << wagi_przedmiotow[i] << "x" << (i+1);
    }
    cout << " <= " << udzwig << endl << endl;
}

bool VectorContains(vector<int> v, int item){
    if(v.empty()) return false;
    for(int i = 0; i < v.size(); ++i){
        if(v[i] == item) return true;
    }
    return false;
}

int Lowerbound(vector<int> &J, vector<int> &K, vector<int> wartosci_przedmiotow, vector<int> wagi_przedmiotow, int udzwig){
    int suma_w = 0, suma_p = 0;
    for(int i = 0; i < J.size(); ++i){
        suma_w += wagi_przedmiotow[J[i]];
        suma_p += wartosci_przedmiotow[J[i]];
    }
    int x = udzwig - suma_w;
    int y = suma_p;
    int n = wartosci_przedmiotow.size(); // ilosc przedmiotow
    for(int i = 0; i < n; ++i){
        if((!VectorContains(J, i) && !VectorContains(K, i)) && (wagi_przedmiotow[i] <= x)){
            J.push_back(i);
            x -= wagi_przedmiotow[i];
            y += wartosci_przedmiotow[i];
        }
        else{
            K.push_back(i);
        } 
    }
    return y;
}

void Plecakov(int k,vector<int> wartosci_przedmiotow, vector<int> wagi_przedmiotow, int udzwig, DrzewoPermutacji *d){
    // k to iteracja
    if(d->Permutacje.empty()) return;
    int Q = 0;
    vector<int> result;
    for(auto element : d->Permutacje){
        vector<int> tmp;
        int lowerbound = Lowerbound(element, tmp, wartosci_przedmiotow, wagi_przedmiotow, udzwig);
        if(Q < lowerbound){
            Q = lowerbound;
            result = element;
        }
        
    }
    int suma = 0;
    for(int i = 0; i < result.size(); ++i){
        suma += wagi_przedmiotow[result[i]];
    }
    if(suma>udzwig) return;
    cout << "Wynik po iteracji k=" << k+1 << endl << "Przedmioty : ";
    for(int i = 0 ; i < result.size(); ++i){
        if(i > 0) cout << " + ";
        cout << wartosci_przedmiotow[result[i]] << "x" << result[i]+1;
        
    }
    cout << endl;
    cout << "Waga plecaka: " << suma << " <= " << udzwig << endl;
    cout << "Lowerbound : ";
    cout << Q; 
    cout << endl << endl;
}

bool compare_pair(const pair<int, double> &p1, const pair<int,double> &p2){
    return p1.second > p2.second;
}

void Sortuj_Wedlug_Ilorazu(vector<int> &wartosc, vector<int> &waga){
    int n = wartosc.size();
    vector<pair<int,double>> tmp;
    for(int i = 0; i < n; ++i){
        double iloraz = (double)wartosc[i]/waga[i];
        tmp.push_back(make_pair(i, iloraz));
    }
    sort(tmp.begin(), tmp.end(), compare_pair);
    vector<int> tmp_wartosc(n), tmp_waga(n);
    for(int i = 0; i < n; ++i){
        tmp_wartosc[i] = wartosc[tmp[i].first];
        tmp_waga[i] = waga[tmp[i].first];
    }
    wartosc = tmp_wartosc;
    waga = tmp_waga;
}

int main(){

    int n=0;
    int udzwig = 0;
    cout << "Podaj ilosc przedmiotow: ";
    cin >> n;
    vector<int> wartosc_przedmiotow(n);
    vector<int> waga_przedmiotu(n);
    for(int i = 0 ; i < n; ++i){
        cout << "Podaj wartosc przedmiotu przy x" << (i+1) << ": ";
        cin >> wartosc_przedmiotow[i];
        cout << "Podaj wage x" << (i+1) << ": ";
        cin >> waga_przedmiotu[i];
    }
    cout << "Podaj udzwig plecaka: ";
    cin >> udzwig;

    cout << "Sortowac wedlug ilorazu wartosci i wagi?(y/n) : ";
    string s;
    cin >> s;
    if(s == "y")
        Sortuj_Wedlug_Ilorazu(wartosc_przedmiotow, waga_przedmiotu);
    
    WyswietlWielomian(wartosc_przedmiotow, waga_przedmiotu, udzwig);

    DrzewoPermutacji *d = new DrzewoPermutacji(n);
    for(int k = 0; k < n; ++k){
        d->Permutacja(k);
        d->MniejszyRownyUdzwig(udzwig, waga_przedmiotu);
        Plecakov(k, wartosc_przedmiotow, waga_przedmiotu, udzwig, d);
    }
    delete d;
}